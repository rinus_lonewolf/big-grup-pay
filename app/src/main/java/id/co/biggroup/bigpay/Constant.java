package id.co.biggroup.bigpay;

/**
 * Created by dolan on 2/21/18.
 */

public interface Constant {
    String DATA_STEP = "step";
    String DATA_FORM = "form";
    String DATA_BP_GROUP = "group";
    String DATA_ITEM = "item";
    String DATA_ITEM_TYPE_BUILDING = "building";
    String DATA_ITEM_TYPE_HOME = "home";
    String DATA_MENU_ITEM = "menu_item";
    String KEY_PREFS = "id.or.muhammadiyah.mpay.PREFERENCES";
    String PREF_TOKEN = "token";
    String PREF_EMAIL = "email";
    String PREF_NAME = "name";
    String PREF_PHONE = "phone";
    String PREF_BALANCE = "balance";
    String PREF_IMAGE = "image";
    String PREF_LANGUAGE = "language";
    int STEP_SCAN = 0;
    int STEP_INPUT = 1;
    int STEP_CONFIRM = 2;
    int STEP_RESULT = 3;
    int KEY_MIN_LENGTH = 8;
    int PAYMENT_CHANNEL = 11;
    String BACK_TO = "back_to";
    int HOME_TAB = 1;
//    int TRANSACTION_TAB = 1;
//    int HISTORY_TAB = 2;
//    int PROFILE_TAB = 3;
    int HISTORY_TAB = 0;
    int PROFILE_TAB = 2;
    String PREFIX_HISTORY_UNIT = "historyUnit_";
    String SERVICE_INQUIRY = "01";
    String SERVICE_PAYMENT = "02";
    int GET_DENOM_PAYOUT = 1;
    int PHOTO_MAX_SIZE = 300;
}
