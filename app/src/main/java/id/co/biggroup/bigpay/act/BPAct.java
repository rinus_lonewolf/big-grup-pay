package id.co.biggroup.bigpay.act;

import android.support.v7.app.AlertDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.adapter.BillPayResponseItemAdapter;
import id.co.biggroup.bigpay.adapter.SpinnerAdapter;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.BillPayRequestBody;
import id.co.biggroup.bigpay.api.request.GetDenomPayoutRequestBody;
import id.co.biggroup.bigpay.api.request.TokenizedRequestBody;
import id.co.biggroup.bigpay.api.response.BalanceResponseBody;
import id.co.biggroup.bigpay.api.response.GetBPResponseBody;
import id.co.biggroup.bigpay.databinding.ActBpConfirmBinding;
import id.co.biggroup.bigpay.databinding.ActBpInputBinding;
import id.co.biggroup.bigpay.databinding.ActBpResultBinding;
import id.co.biggroup.bigpay.enums.BillPaymentGroup;
import id.co.biggroup.bigpay.enums.BillPaymentType;
import id.co.biggroup.bigpay.enums.Biller;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.enums.SubBiller;
import id.co.biggroup.bigpay.form.BPForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.model.SpinnerItem;
import id.co.biggroup.bigpay.model.SpinnerItemImpl;
import id.co.biggroup.bigpay.util.CommonBindingAdapter;
import id.co.biggroup.bigpay.util.CommonUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

public class BPAct extends NavBarAct implements WidgetEventHandler, CommonBindingAdapter.Listener,
        Response.Listener<GetBPResponseBody> {

  private final static String TAG = BPAct.class.getName();
  private final static SpinnerItemImpl[] months = new SpinnerItemImpl[12];
  private ActBpInputBinding inputBinding;
  private ActBpConfirmBinding confirmBinding;
  private ActBpResultBinding resultBinding;
  private int step;
  private int type = 0;
  private BPForm form;

  static {
        for (int i = 0; i < 12; i++) {
            int val = i+1;
            months[i] = new SpinnerItemImpl(val, String.valueOf(val) + " bulan");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        step = getIntent().getIntExtra(Constant.DATA_STEP, Constant.STEP_INPUT);
        if (step == Constant.STEP_INPUT) {
            inputBinding = DataBindingUtil.setContentView(this, R.layout.act_bp_input);

            BillPaymentGroup group = (BillPaymentGroup) getIntent().getSerializableExtra(Constant.DATA_BP_GROUP);
            inputBinding.billPaymentType.setAdapter(new SpinnerAdapter(this, group.getTypes()));

            BillPaymentType type = (BillPaymentType) inputBinding.billPaymentType.getSelectedItem();
            inputBinding.biller.setAdapter(new SpinnerAdapter(this, type.getBillers()));
            inputBinding.subBiller.setAdapter(new SpinnerAdapter(this));
            inputBinding.month.setAdapter(new SpinnerAdapter(this, months));
            if (type == BillPaymentType.CREDIT_CARD) {
                inputBinding.inputDescription.setFilters(new InputFilter[] {new InputFilter.LengthFilter(19)});
                inputBinding.inputDescription.addTextChangedListener(new TextWatcher() {
                private static final char space = ' ';
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) { }
                @Override
                public void afterTextChanged(Editable s) {
                  // Remove spacing char
                  if (s.length() > 0 && (s.length() % 5) == 0) {
                    final char c = s.charAt(s.length() - 1);
                    if (space == c)
                      s.delete(s.length() - 1, s.length());
                  }
                  // Insert char where needed.
                  if (s.length() > 0 && (s.length() % 5) == 0) {
                    char c = s.charAt(s.length() - 1);
                    // Only if its a digit where there should be a space we insert a space
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3)
                      s.insert(s.length() - 1, String.valueOf(space));
                  }
                }
              });
            }

            Biller biller = (Biller) inputBinding.biller.getSelectedItem();
            inputBinding.optDenom.setAdapter(new SpinnerAdapter(this, getDenomItems(biller)));

            form = new BPForm();
            form.setGroup(group);
            form.setType(type);
            form.setBiller(biller);
            inputBinding.setForm(form);
            inputBinding.setHandler(this);
            inputBinding.setListener(this);

            if (getSupportActionBar() != null) {
              getSupportActionBar().setTitle(group.getLabelResourceId());
            }
        } else if (step == Constant.STEP_CONFIRM) {
            confirmBinding = DataBindingUtil.setContentView(this, R.layout.act_bp_confirm);
            form = (BPForm) getIntent().getSerializableExtra(Constant.DATA_FORM);
            confirmBinding.setForm(form);
            confirmBinding.setHandler(this);
            confirmBinding.button.setVisibility(View.GONE);

            //Check balance apakah cukup untuk membayar tagihan? jika tidak, hilangkan tombol bayar
            Response.Listener<BalanceResponseBody> responseListener = response -> {
              if (response != null) {
                if (response.balance < form.total) {
                  confirmBinding.button.setVisibility(View.GONE);
                  Toast.makeText(BPAct.this, R.string.notif_insufficient_balance,
                    Toast.LENGTH_LONG).show();
                }
                else {
                  confirmBinding.button.setVisibility(View.VISIBLE);
                }
              }
            };

            TokenizedRequestBody body = new TokenizedRequestBody();
            body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
            APIRequest<BalanceResponseBody> request = new APIRequest<>(this,
              APIRequest.Instance.GET_BALANCE, body, responseListener, this);
            queue = Volley.newRequestQueue(this);
            queue.add(request);

            //Show inquiry result from API on confirm page
            if (!form.getBiller().isNonInquiry()) {
              BillPayResponseItemAdapter adapter =
                new BillPayResponseItemAdapter(this, decodeBillPaymentDetail(form.items));
              confirmBinding.viewList.setAdapter(adapter);
            }
        }
        else if (step == Constant.STEP_RESULT) {
          resultBinding = DataBindingUtil.setContentView(this, R.layout.act_bp_result);
          form = (BPForm) getIntent().getSerializableExtra(Constant.DATA_FORM);
          resultBinding.setForm(form);
          resultBinding.setHandler(this);

          if (form.items != null)
          {
            BillPayResponseItemAdapter adapter =
              new BillPayResponseItemAdapter(this, decodeBillPaymentDetail(form.items));
            resultBinding.viewList.setAdapter(adapter);
          }
        }
    }

    @Override
    public void onClick(View view) {
        if (step == Constant.STEP_INPUT) {
          hideInputProgress(false);
          boolean valid = true;
          String msg = getString(R.string.field_required);
          BPForm form = inputBinding.getForm();

          if ((inputBinding.inputDescription.getText().toString().length() < 19) &&
            (form.getType() == BillPaymentType.CREDIT_CARD)) {
            inputBinding.inputDescription.setError(getString(R.string.field_cc_not_valid));
            valid = false;
          }
          if (inputBinding.inputDescription.getText().toString().length() == 0) {
            inputBinding.inputDescription.setError(msg);
            valid = false;
          }
          if ((inputBinding.inputAmount.getVisibility() == View.VISIBLE) &&
            (inputBinding.inputAmount.getText().toString().length() == 0)) {
            inputBinding.inputAmount.setError(msg);
            valid = false;
          }
          if (!valid) {
            hideInputProgress(true);
            return;
          }

          if (form.getDenom() != null)
            form.denominasi = Long.valueOf(form.getDenom().getLabel());

          if (form.getBiller().isNonInquiry()) {
            //Cek harga pulsa operator untuk setiap denominasi
            if (form.getType() == BillPaymentType.PREPAID_CELLPHONE) {
              GetDenomPayoutRequestBody body = new GetDenomPayoutRequestBody();
              body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
              body.institutionCode = form.getBiller().getCode();
              body.denom = form.denominasi;
              APIRequest<GetBPResponseBody> request = new APIRequest<>(this,
                APIRequest.Instance.GET_DENOM_PAYOUT, body, this, this);
              queue = Volley.newRequestQueue(this);
              queue.add(request);

              type = Constant.GET_DENOM_PAYOUT;
              return;
            }
            else
              form.total = form.getAmount();

            Intent i = new Intent(this, BPAct.class);
            i.putExtra(Constant.DATA_STEP, step + 1);
            i.putExtra(Constant.DATA_FORM, form);
            startActivity(i);

            hideInputProgress(true);
          }
          else {
            //Special for Open Amount BIG TV
            if (form.getSubBiller() != null && form.getSubBiller().isOpenAmount())
              form.total = form.getAmount();

            BillPayRequestBody body = new BillPayRequestBody();
            body.code = Constant.SERVICE_INQUIRY;
            body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
            body.amount = (long) 0;
            if (form.getBiller().hasSubBillers())
              body.institutionCode = form.getSubBiller().getCode();
            else
              body.institutionCode = form.getBiller().getCode();
            if (form.getBiller() == Biller.S_BPJS_SEHAT)
              body.billNo = form.getBillId() + ";" + form.getMonth().getLabel();
            else
              body.billNo = form.getBillId().replaceAll("\\s+","");
            APIRequest<GetBPResponseBody> request = new APIRequest<>(this,
              APIRequest.Instance.TRX_BILL_PAY, body, this, this);
            queue = Volley.newRequestQueue(this);
            queue.add(request);
          }
//            Log.d(TAG, String.format("selected type: %s, biller: %s, subBiller: %s, amount: %d", //, save: %s
//              form.getType() != null ? form.getType().getName() : "null",
//              form.getBiller() != null ? form.getBiller().getName() : "null",
//              form.getSubBiller() != null ? form.getSubBiller().getName() : "null",
//              form.getAmount() != null ? form.getAmount() : 0));
          return;
        }
        if (step == Constant.STEP_CONFIRM) {
          //Dialog confirmation Yes/No
          new AlertDialog.Builder(this)
            .setTitle(R.string.label_trx_confirmation)
            .setMessage(R.string.notif_trx_confirmation_question)
            .setIcon(R.drawable.ic_big_group)
            .setCancelable(false)
            .setPositiveButton(R.string.label_yes, (dialog, which) -> {
              dialog.dismiss();
              confirmBinding.progressBar.setVisibility(View.VISIBLE);
              BPForm form = this.form;
              BillPayRequestBody body = new BillPayRequestBody();
              body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
              body.code = Constant.SERVICE_PAYMENT;
              body.institutionCode = form.getBiller().getCode();
              body.amount = form.total;
              body.refNo = CommonUtil.formatReferenceDateTime(new Date());
              if (form.getBiller() == Biller.S_BPJS_SEHAT)
                body.billNo = form.getBillId() + ";" + form.getMonth().getLabel() + ";" + form.getAuxBillId();
              else
                body.billNo = form.getBillId().replaceAll("\\s+","");

              Log.d(TAG, body.code + " ; " + body.institutionCode + " ; " + body.billNo + " ; " + body.amount);

              APIRequest<GetBPResponseBody> request = new APIRequest<>(this,
                APIRequest.Instance.TRX_BILL_PAY, body, this, this);
              queue = Volley.newRequestQueue(this);
              queue.add(request);
            })
            .setNegativeButton(R.string.label_no, (dialog, i) -> dialog.dismiss())
            .show();

          confirmBinding.progressBar.setVisibility(View.GONE);
          return;
        }
        if (step == Constant.STEP_RESULT) {
          Intent i = new Intent(this, MainAct.class);
          i.putExtra(Constant.BACK_TO, Constant.HISTORY_TAB);
          i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
          startActivity(i);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.equals(inputBinding.billPaymentType)) {
            BillPaymentType type = (BillPaymentType) inputBinding.billPaymentType.getSelectedItem();
            SpinnerAdapter adapter = (SpinnerAdapter) inputBinding.biller.getAdapter();
            adapter.clear();
            adapter.addAll(type.getBillers());
            adapter.notifyDataSetChanged();
            inputBinding.biller.setSelection(0);
            inputBinding.getForm().setBiller((Biller) inputBinding.biller.getSelectedItem());
        }
        if (parent.equals(inputBinding.biller)) {
            Biller biller = (Biller) inputBinding.biller.getSelectedItem();
            SpinnerAdapter adapter = (SpinnerAdapter) inputBinding.subBiller.getAdapter();
            adapter.clear();
            adapter.addAll(biller.getSubBillers());
            adapter.notifyDataSetChanged();

            if (biller.getSubBillers().length > 0) {
                inputBinding.subBiller.setSelection(0);
                inputBinding.getForm().setSubBiller((SubBiller) inputBinding.subBiller.getSelectedItem());
            } else {
                inputBinding.getForm().setSubBiller(null);
            }
            if (biller.hasDenoms()) {
                adapter = (SpinnerAdapter) inputBinding.optDenom.getAdapter();
                adapter.clear();
                adapter.addAll(getDenomItems(biller));
                adapter.notifyDataSetChanged();
                inputBinding.optDenom.setSelection(0);
                inputBinding.getForm().setDenom((SpinnerItem) inputBinding.optDenom.getSelectedItem());
            } else {
                inputBinding.getForm().setDenom(null);
            }
        }
//        if (parent.equals(inputBinding.optSaved)) {
//            Log.d(TAG, "favorite combo box selected");
//        }
    }

    private List<SpinnerItem> getDenomItems(Biller biller) {
        List<SpinnerItem> items = new ArrayList<>();
        if (!biller.hasDenoms()) {
            return items;
        }
        for (Integer denom : biller.getDenoms()) {
            items.add(new SpinnerItemImpl(denom, denom.toString()));
        }
        return items;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        if (step == Constant.STEP_INPUT) {
          hideInputProgress(true);
        }
        else if (step == Constant.STEP_CONFIRM)
          confirmBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(GetBPResponseBody response) {
        if (step == Constant.STEP_INPUT) {
          hideInputProgress(true);
            ResponseStatus status = ResponseStatus.getByValue(response.status);
            if (!Status.SUCCESS_BILLPAY.equals(status.getStatus())) {
                Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            }
            else {
              BPForm form = inputBinding.getForm();
              if (type != Constant.GET_DENOM_PAYOUT) {
                form.items = response.items.toString();
                Log.d("ITEMS INQUIRY", form.items);
              }

              if (response.feeAmount != null) {
                if (response.payAmount == null)
                  form.total = form.denominasi + response.feeAmount;
                else
                  form.total = response.payAmount + response.feeAmount;
              }
              else
                form.total = response.payAmount;

              Intent i = new Intent(this, BPAct.class);
              i.putExtra(Constant.DATA_STEP, step + 1);
              i.putExtra(Constant.DATA_FORM, form);
              startActivity(i);
            }
        }
        else if (step == Constant.STEP_CONFIRM) {
          confirmBinding.progressBar.setVisibility(View.GONE);
          ResponseStatus status = ResponseStatus.getByValue(response.status);
          if (!Status.SUCCESS_BILLPAY.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
          }
          else {
            BPForm form = confirmBinding.getForm();
            if (response.items != null)
              form.items = response.items.toString();
            else
              form.items = null;

            Intent i = new Intent(this, BPAct.class);
            i.putExtra(Constant.DATA_STEP, step + 1);
            i.putExtra(Constant.DATA_FORM, form);
            startActivity(i);

//            new AlertDialog.Builder(this)
//              .setIcon(R.mipmap.logo_splash)
//              .setCancelable(false)
//              .setTitle(status.getMessageId())
//              .setMessage(R.string.notif_trx_success)
//              .setPositiveButton(R.string.ok, (dialog, which) -> {
//                dialog.dismiss();
//                Intent i = new Intent(this, MainAct.class);
//                i.putExtra(Constant.BACK_TO, Constant.HISTORY_TAB);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(i);
//              }).show();
          }
        }
    }

    private HashMap<String, String> decodeBillPaymentDetail(String jsonItems)
    {
      HashMap<String, String> responseItems = new HashMap<>();
      try
      {
        JSONObject jsonObject = new JSONObject(jsonItems);
        Iterator<?> keys = jsonObject.keys();
        while (keys.hasNext()) {
          try {
            String key = (String) keys.next();
            String value = jsonObject.get(key).toString();
            responseItems.put(key, value);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      catch (JSONException e) {
        e.printStackTrace();
      }
      return responseItems;
    }

    private void hideInputProgress(boolean flag) {
      inputBinding.buttonNext.setEnabled(flag);
      if (flag)
        inputBinding.progressBar.setVisibility(View.INVISIBLE);
      else
        inputBinding.progressBar.setVisibility(View.VISIBLE);
    }
}
