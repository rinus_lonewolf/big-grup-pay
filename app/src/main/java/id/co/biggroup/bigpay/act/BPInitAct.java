package id.co.biggroup.bigpay.act;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TableLayout;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.enums.BillPaymentGroup;
import id.co.biggroup.bigpay.util.WidgetHelper;

public class BPInitAct extends NavBarAct {

    private BillPaymentGroup[] items_1 = new BillPaymentGroup[] {
            BillPaymentGroup.RESIDENCE_BILL, BillPaymentGroup.INSURANCE,
            BillPaymentGroup.ENTERTAINMENT, BillPaymentGroup.TRAVEL,
            BillPaymentGroup.CREDIT_CARD, BillPaymentGroup.LOAN,
    };
    private BillPaymentGroup[] items_2 = new BillPaymentGroup[] {
            BillPaymentGroup.PREPAID_CELLPHONE, BillPaymentGroup.POSTPAID_CELLPHONE,
            BillPaymentGroup.PREPAID_ELECTRIC,
    };

    private WidgetHelper.Listener listener = new WidgetHelper.Listener<BillPaymentGroup>() {
        @Override
        public void onItemSelected(BillPaymentGroup item) {
            Intent intent = new Intent(BPInitAct.this, BPAct.class);
            intent.putExtra(Constant.DATA_BP_GROUP, item);
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_bp_init);

        TableLayout layout = findViewById(R.id.layout_table_1);
        WidgetHelper.fill(layout, items_1, listener, R.string.title_bill, R.drawable.bg_plain);
        layout = findViewById(R.id.layout_table_2);
        WidgetHelper.fill(layout, items_2, listener, R.string.title_mobile_and_electric, 0);
    }
}
