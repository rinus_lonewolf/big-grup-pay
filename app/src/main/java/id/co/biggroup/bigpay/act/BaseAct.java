package id.co.biggroup.bigpay.act;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.util.CommonUtil;

/**
 * Created by developer on 3/22/18.
 */

public abstract class BaseAct extends AppCompatActivity implements Response.ErrorListener {
    protected RequestQueue queue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (this instanceof InitAct) {
            overridePendingTransition(R.anim.zoom_in, R.anim.fade_out);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (this instanceof InitAct) {
            overridePendingTransition(R.anim.fade_in, R.anim.zoom_out);
        } else {
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, CommonUtil.getErrorMessageResource(error), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(request -> true);
        }
    }
}
