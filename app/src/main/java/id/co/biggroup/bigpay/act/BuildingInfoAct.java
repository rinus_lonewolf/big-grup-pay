package id.co.biggroup.bigpay.act;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.adapter.GridMenuAdapter;
import id.co.biggroup.bigpay.databinding.ActBuildingInfoBinding;
import id.co.biggroup.bigpay.enums.BuildingInfoItem;

public class BuildingInfoAct extends NavBarAct
{
  private final static BuildingInfoItem[] itemsHotel = new BuildingInfoItem[] {
    BuildingInfoItem.MAJESTY_HOTEL, BuildingInfoItem.GRAND_SETIABUDI_HOTEL,
    BuildingInfoItem.GALERI_CIUMBULEUIT_HOTEL, BuildingInfoItem.BTC_HOTEL,
    BuildingInfoItem.GREEN_FOREST
  };
  private final static BuildingInfoItem[] itemsResidential = new BuildingInfoItem[] {
    BuildingInfoItem.HEGARMANAH, BuildingInfoItem.GALERI_CIUMBULEUIT_2,
    BuildingInfoItem.GALERI_CIUMBULEUIT_3, BuildingInfoItem.PINEWOOD
  };
  private final static BuildingInfoItem[] itemsMall = new BuildingInfoItem[] {
    BuildingInfoItem.BTC_FASHION_MALL, BuildingInfoItem.SOLO_GRAND_MALL,
    BuildingInfoItem.SOLO_PARAGON, BuildingInfoItem.JATINANGOR_TOWN_SQUARE
  };

  private ActBuildingInfoBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.act_building_info);

    binding.hotelGridView.setAdapter(new GridMenuAdapter(this, itemsHotel));
    binding.hotelGridView.setOnItemClickListener((adapterView, view, i, l) -> {
      BuildingInfoItem item = (BuildingInfoItem) adapterView.getAdapter().getItem(i);
      openWebView(item);
    });

    binding.residentialGridView.setAdapter(new GridMenuAdapter(this, itemsResidential));
    binding.residentialGridView.setOnItemClickListener((adapterView, view, i, l) -> {
      BuildingInfoItem item = (BuildingInfoItem) adapterView.getAdapter().getItem(i);
      openWebView(item);
    });

    binding.mallGridView.setAdapter(new GridMenuAdapter(this, itemsMall));
    binding.mallGridView.setOnItemClickListener((adapterView, view, i, l) -> {
      BuildingInfoItem item = (BuildingInfoItem) adapterView.getAdapter().getItem(i);
      openWebView(item);
    });

    if (getSupportActionBar() != null)
    {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    if (item.getItemId() == android.R.id.home)
    {
      finish();
    }
    return super.onOptionsItemSelected(item);
  }

  private void openWebView(BuildingInfoItem item)
  {
    Intent intent = new Intent(this, WebViewAct.class);
    intent.putExtra(Constant.DATA_ITEM, Constant.DATA_ITEM_TYPE_BUILDING);
    intent.putExtra(Constant.DATA_MENU_ITEM, item);
    startActivity(intent);
  }
}
