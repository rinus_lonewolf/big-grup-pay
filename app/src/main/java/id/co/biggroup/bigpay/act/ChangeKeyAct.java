package id.co.biggroup.bigpay.act;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.ChangeKeyRequestBody;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.databinding.ActChangeKeyBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.ChangeKeyForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;
import id.co.biggroup.bigpay.util.ValidationUtil;

public class ChangeKeyAct extends NavBarAct implements WidgetEventHandler, Response.Listener<APIResponseBody> {
    private ActChangeKeyBinding binding;
    private ValidationUtil validationUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.act_change_key);
        binding.setForm(new ChangeKeyForm());
        binding.setHandler(this);
        validationUtil = new ValidationUtil(this);
    }

    @Override
    public void onClick(View view) {
        if (binding.progressBar.getVisibility() == View.VISIBLE) {
            return;
        }
        if (!validate()) {
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        ChangeKeyForm form = binding.getForm();
        ChangeKeyRequestBody body = new ChangeKeyRequestBody();
        body.oldPassword = AuthUtil.digest(form.getOldKey());
        body.newPassword = AuthUtil.digest(form.getNewKey());
        body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
        APIRequest<APIResponseBody> request = new APIRequest<>(this,
                APIRequest.Instance.CHANGE_KEY, body, this, this);
        queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private boolean validate() {
        boolean valid = validationUtil.isFilled(binding.inputOldKey);
        if (!validationUtil.isFilled(binding.inputNewKey)) {
            valid &= false;
        } else if (!validationUtil.isLengthValid(binding.inputNewKey, ValidationUtil.LengthType.MIN, Constant.KEY_MIN_LENGTH)) {
            valid &= false;
        } else {
            valid &= validationUtil.isPatternValid(binding.inputNewKey, ValidationUtil.PASSWORD_PATTERN);
        }
        valid &= validationUtil.isEqual(binding.inputConfirmNewKey, binding.inputNewKey);
        valid &= validationUtil.isKeysDiffer(binding.inputNewKey, binding.inputOldKey);

        return valid;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        binding.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(APIResponseBody response) {
        binding.progressBar.setVisibility(View.INVISIBLE);
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, R.string.notif_success_changekey, Toast.LENGTH_SHORT).show();
        finish();
    }
}
