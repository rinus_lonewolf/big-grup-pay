package id.co.biggroup.bigpay.act;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.ResetKeyRequestBody;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.databinding.ActForgotKeyBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.SigninForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.ValidationUtil;

public class ForgotKeyAct extends BaseAct implements WidgetEventHandler, Response.Listener<APIResponseBody> {

    private ActForgotKeyBinding binding;
    private ValidationUtil validationUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBarWhite);
        binding = DataBindingUtil.setContentView(this, R.layout.act_forgot_key);
        binding.setForm(new SigninForm());
        binding.setHandler(this);
        validationUtil = new ValidationUtil(this);
    }

    @Override
    public void onClick(View view) {
        if (!validate()) {
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        ResetKeyRequestBody body = new ResetKeyRequestBody();
        body.email = binding.getForm().getEmail();
        APIRequest request = new APIRequest(this, APIRequest.Instance.RESET_KEY, body, this, this);
        queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private boolean validate() {
        boolean valid = true;
        if (!validationUtil.isFilled(binding.inputEmail)) {
            valid &= false;
        } else {
            valid &= validationUtil.isPatternValid(binding.inputEmail, ValidationUtil.EMAIL_PATTERN);
        }
        return valid;
    }

    @Override
    public void onResponse(APIResponseBody response) {
        binding.progressBar.setVisibility(View.INVISIBLE);
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, R.string.notif_success_resetkey, Toast.LENGTH_SHORT).show();
        finish();
    }
}
