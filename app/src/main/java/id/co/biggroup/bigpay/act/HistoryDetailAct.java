package id.co.biggroup.bigpay.act;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.response.BPTrx;
import id.co.biggroup.bigpay.api.response.VATrx;
import id.co.biggroup.bigpay.databinding.ActBpHistoryDetailBinding;
import id.co.biggroup.bigpay.databinding.ActVaHistoryDetailBinding;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;

public class HistoryDetailAct extends NavBarAct implements WidgetEventHandler {

  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getSerializableExtra(Constant.DATA_ITEM) instanceof BPTrx) {
          ActBpHistoryDetailBinding bpBinding = DataBindingUtil.setContentView(this, R.layout.act_bp_history_detail);
          BPTrx bpItem = (BPTrx) getIntent().getSerializableExtra(Constant.DATA_ITEM);
          bpBinding.setBPItem(bpItem);
          bpBinding.setHandler(this);
        }
        else {
          ActVaHistoryDetailBinding vaBinding = DataBindingUtil.setContentView(this, R.layout.act_va_history_detail);
          VATrx vaItem = (VATrx) getIntent().getSerializableExtra(Constant.DATA_ITEM);
          vaBinding.setVAItem(vaItem);
          vaBinding.setHandler(this);
        }
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
