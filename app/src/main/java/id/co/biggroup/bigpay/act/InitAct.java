package id.co.biggroup.bigpay.act;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.SigninExRequestBody;
import id.co.biggroup.bigpay.api.request.SignupExRequestBody;
import id.co.biggroup.bigpay.api.request.UpdateProfileRequestBody;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.api.response.SigninResponseBody;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.SignUpForm;
import id.co.biggroup.bigpay.util.CommonUtil;
import id.co.biggroup.bigpay.util.LanguageUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

public class InitAct extends BaseAct implements Response.Listener<SigninResponseBody>
{
  private GoogleSignInClient mGoogleSignInClient;
  private CallbackManager mCallbackManager;
  private SignUpForm form;
  private SignupExRequestBody signupExRequestBody = null;
  private UpdateProfileRequestBody profileRequestBody = null;
  private Response.Listener<APIResponseBody> apiResponseListener;
  private Target target;
  private ProgressDialog progressDialog;
  private Runnable task = () -> {
    progressDialog.dismiss();
    GoogleSignIn();
  };

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setTheme(R.style.AppTheme_NoActionBarWhite);
    setContentView(R.layout.act_init);

    // -- GOOGLE --
    // Configure sign-in to request the user's ID, email address, and basic
    // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
      .requestEmail()
      .build();

    // Build a GoogleSignInClient with the options specified by gso.
    mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    // Set the dimensions of the Google sign-in button.
    findViewById(R.id.signinGoogleButton).setOnClickListener(view -> GoogleSignIn());

    // -- FACEBOOK --
    // Initialize Facebook Login button
    mCallbackManager = CallbackManager.Factory.create();

      try {
          PackageInfo info = getPackageManager().getPackageInfo(
                  "id.co.biggroup.bigpay",
                  PackageManager.GET_SIGNATURES);
          for (Signature signature : info.signatures) {
              MessageDigest md = MessageDigest.getInstance("SHA");
              md.update(signature.toByteArray());
              Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
          }
      } catch (PackageManager.NameNotFoundException e) {

      } catch (NoSuchAlgorithmException e) {

      }

    LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>()
      {
        @Override
        public void onSuccess(LoginResult loginResult)
        {
          GraphRequest request = GraphRequest.newMeRequest(
            loginResult.getAccessToken(), (object, response) -> {
              try
              {
                String email = object.getString("email");
                String name = object.getString("name");
                if (email != null && !email.equals("") && name != null && !name.equals(""))
                  updateUI(name, email, loginResult.getAccessToken().getUserId());
                else
                  Toast.makeText(InitAct.this,
                    getString(R.string.info_empty_facebook_email), Toast.LENGTH_SHORT).show();
              }
              catch (JSONException e)
              {
                e.printStackTrace();
              }
            });
          Bundle parameters = new Bundle();
          parameters.putString("fields", "id,name,email");
          request.setParameters(parameters);
          request.executeAsync();
        }

        @Override
        public void onCancel()
        {
        }

        @Override
        public void onError(FacebookException exception)
        {
          Log.d("InitAct","exception : "+exception);
        }
      }
    );
    findViewById(R.id.signinFacebookButton).setOnClickListener(view -> FacebookSignIn());

    findViewById(R.id.signinButton).setOnClickListener(view -> {
      Intent i = new Intent(InitAct.this, SigninAct.class);
      startActivity(i);
    });
    findViewById(R.id.signupButton).setOnClickListener(view -> {
      Intent i = new Intent(InitAct.this, SignUpAct.class);
      startActivity(i);
    });

    View langIdLayout = findViewById(R.id.languageIdLayout);
    View langEnLayout = findViewById(R.id.languageEnLayout);
    Button langIdBtn = findViewById(R.id.languageIdBtn);
    Button langEnBtn = findViewById(R.id.languageEnBtn);

    langIdBtn.setOnClickListener(v -> {
      if (!Locale.getDefault().getLanguage().equals(LanguageUtil.Indonesian))
      {
        PreferencesUtil.save(this, Constant.PREF_LANGUAGE, LanguageUtil.Indonesian);
        LanguageUtil.updateBaseContextLocale(this, LanguageUtil.Indonesian);
        restartActivity();
      }
    });

    langEnBtn.setOnClickListener(v -> {
      if (!Locale.getDefault().getLanguage().equals(LanguageUtil.English))
      {
        PreferencesUtil.save(this, Constant.PREF_LANGUAGE, LanguageUtil.English);
        LanguageUtil.updateBaseContextLocale(this, LanguageUtil.English);
        restartActivity();
      }
    });

    LanguageUtil.checkInitLanguage(this, langIdBtn, langEnBtn, langIdLayout, langEnLayout);

    target = new Target()
    {
      @Override
      public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
      {
        String strImage = CommonUtil.encodeToBase64(CommonUtil.resizeBitmap(bitmap, Constant.PHOTO_MAX_SIZE));

        Log.d("InitAct","strImage : "+strImage);
        if (strImage != null && !strImage.equals(""))
        {
          profileRequestBody = new UpdateProfileRequestBody();
          profileRequestBody.token = PreferencesUtil.getString(InitAct.this, Constant.PREF_TOKEN);
          profileRequestBody.name = form.getName();
          profileRequestBody.image = strImage;

          PreferencesUtil.save(InitAct.this, Constant.PREF_IMAGE, strImage);

          APIRequest<APIResponseBody> request = new APIRequest<>(InitAct.this,
            APIRequest.Instance.UPDATE_PROFILE, profileRequestBody, apiResponseListener, InitAct.this);
          queue = Volley.newRequestQueue(InitAct.this);
          queue.add(request);
          Log.d("API REQ", "UPDATE PROFILE PICTURE");
        }
        else
          gotoMainMenu();
      }

      @Override
      public void onBitmapFailed(Exception e, Drawable errorDrawable)
      {
        Log.d("InitAct","onBitmapFailed : "+e);
        Log.d("InitAct","onBitmapFailed2 : "+errorDrawable);
        gotoMainMenu();
      }

      @Override
      public void onPrepareLoad(Drawable placeHolderDrawable)
      {
        Log.d("InitAct","onPrepareLoad : "+placeHolderDrawable);
//        gotoMainMenu();
      }
    };

    apiResponseListener = response -> {
      if (response != null)
      {
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (!Status.SUCCESS.equals(status.getStatus()))
          Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
        else
        {
          if (profileRequestBody != null)
            gotoMainMenu();
          else
          {
            Toast.makeText(this, R.string.notif_success_signup, Toast.LENGTH_SHORT).show();
            SigninExRequestBody body = new SigninExRequestBody();
            body.email = form.getEmail();
            body.source = form.getSource();
            APIRequest<SigninResponseBody> request = new APIRequest<>(this,
              APIRequest.Instance.SIGN_IN_EX, body, this, this);
            queue = Volley.newRequestQueue(this);
            queue.add(request);
            Log.d("API REQ", "SIGN IN EXTERNAL AFTER REGISTRATION");
          }
        }
      }
    };
  }

  @Override
  public void onDestroy()
  {
    Picasso.get().cancelRequest(target);
    super.onDestroy();
  }

  private void GoogleSignIn()
  {
    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
    startActivityForResult(signInIntent, 100);
  }

  private void FacebookSignIn()
  {
    LoginManager.getInstance().logInWithReadPermissions(this,
      Arrays.asList("email", "public_profile")
    );
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);

    // Receive callback from Facebook
    mCallbackManager.onActivityResult(requestCode, resultCode, data);

    // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
    if (requestCode == 100)
    {
      // The Task returned from this call is always completed, no need to attach
      // a listener.
      Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
      handleSignInResult(task);
    }
  }

  private void handleSignInResult(Task<GoogleSignInAccount> completedTask)
  {
    try
    {
      GoogleSignInAccount account = completedTask.getResult(ApiException.class);

      // Signed in successfully, show authenticated UI.
      updateUI(account);
    }
    catch (ApiException e)
    {
      // The ApiException status code indicates the detailed failure reason.
      // Please refer to the GoogleSignInStatusCodes class reference for more information.
      Log.d(InitAct.this.getTitle().toString(), "signInResult:failed code=" + e.getStatusCode());
    }
  }

  private void updateUI(GoogleSignInAccount account)
  {
    if (account != null)
    {
      form = new SignUpForm();
      form.setEmail(account.getEmail());
      form.setName(account.getDisplayName());
      Log.d("InitAct","account.getPhotoUrl() : "+account.getPhotoUrl());
      if (account.getPhotoUrl() != null)
        form.setPhoto(account.getPhotoUrl().toString());
      form.setSource("G"); //for google

      SigninExRequestBody body = new SigninExRequestBody();
      body.email = account.getEmail();
      body.source = form.getSource();
      APIRequest<SigninResponseBody> request = new APIRequest<>(this,
        APIRequest.Instance.SIGN_IN_EX, body, this, this);
      queue = Volley.newRequestQueue(this);
      queue.add(request);
      Log.d("API REQ", "SIGN IN EXTERNAL GOOGLE");
    }
  }

  private void updateUI(String name, String email, String userId)
  {
    form = new SignUpForm();
    form.setEmail(email);
    form.setName(name);
    Log.d("InitAct","userId : "+userId);
    form.setPhoto("http://graph.facebook.com/" + userId + "/picture?type=large");
    form.setSource("F"); //for facebook

    SigninExRequestBody body = new SigninExRequestBody();
    body.email = form.getEmail();
    body.source = form.getSource();
    APIRequest<SigninResponseBody> request = new APIRequest<>(this,
      APIRequest.Instance.SIGN_IN_EX, body, this, this);
    queue = Volley.newRequestQueue(this);
    queue.add(request);
    Log.d("API REQ", "SIGN IN EXTERNAL FACEBOOK");
  }

  private void restartActivity()
  {
    Intent intent = new Intent(this, InitAct.class);
    startActivity(intent); // start same activity
    finish(); // destroy older activity
    overridePendingTransition(0, 0); // this is important for seamless transition
  }

  @Override
  public void onResponse(SigninResponseBody response)
  {
    ResponseStatus status = ResponseStatus.getByValue(response.status);
    if (ResponseStatus.SIGN_IN_UNREGISTERED.equals(status))
    {
      Log.d("InitAct","1");
      if (form.getName() != null)
      {
        Log.d("InitAct","2");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View view = inflater.inflate(R.layout.input_custom_dialog, null);
        EditText editText = view.findViewById(R.id.edit_phone);

        builder.setView(view)
          .setNegativeButton(R.string.label_cancel, (dialogInterface, i) -> dialogInterface.dismiss())
          .setPositiveButton(R.string.label_submit, (dialogInterface, i) -> {
            signupExRequestBody = new SignupExRequestBody();
            signupExRequestBody.name = form.getName();
            signupExRequestBody.email = form.getEmail();
            signupExRequestBody.phone = editText.getText().toString();
            signupExRequestBody.source = form.getSource();
            APIRequest<APIResponseBody> request = new APIRequest<>(InitAct.this,
              APIRequest.Instance.SIGN_UP_EX, signupExRequestBody, apiResponseListener, InitAct.this);
            queue = Volley.newRequestQueue(InitAct.this);
            queue.add(request);
            Log.d("API REQ", "SIGN UP EXTERNAL");
            dialogInterface.dismiss();
          })
          .setCancelable(false)
          .create().show();
      }
      else
      {
        Log.d("InitAct","3");
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.info_wait_google_sync));
        progressDialog.show();

        Handler handler = new Handler();
        handler.postDelayed(task, 25000);
      }
    }
    else if (!Status.SUCCESS.equals(status.getStatus()))
      Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
    else
    {
      Map<String, Object> values = new HashMap<>();
      values.put(Constant.PREF_TOKEN, response.token);
      values.put(Constant.PREF_EMAIL, response.email);
      PreferencesUtil.save(this, values);

      Log.d("InitAct","signupExRequestBody : "+signupExRequestBody);
      Log.d("InitAct","form.getPhoto() : "+form.getPhoto());
      if (form.getPhoto() != null && !form.getPhoto().equals(""))
        Picasso.get().load(form.getPhoto()).into(target);
      else
        gotoMainMenu();
//      if (signupExRequestBody != null)
//      {
//        Log.d("InitAct","form.getPhoto() : "+form.getPhoto());
//        if (form.getPhoto() != null && !form.getPhoto().equals(""))
//          Picasso.get().load(form.getPhoto()).into(target);
//        else
//          gotoMainMenu();
//      }
//      else
//        gotoMainMenu();
    }
  }

  private void gotoMainMenu()
  {
    Intent i = new Intent(this, MainAct.class);
    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(i);
  }
}
