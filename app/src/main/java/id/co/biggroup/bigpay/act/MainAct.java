package id.co.biggroup.bigpay.act;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;

import java.util.HashMap;
import java.util.Map;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.TokenizedRequestBody;
import id.co.biggroup.bigpay.api.response.ProfileResponseBody;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.fragment.HistoryFrag;
import id.co.biggroup.bigpay.fragment.MenuFrag;
import id.co.biggroup.bigpay.fragment.ProfileFrag;
import id.co.biggroup.bigpay.util.PreferencesUtil;

public class MainAct extends BaseAct implements Response.Listener<ProfileResponseBody> {

    private final static String FRAGMENT = "frag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        AHBottomNavigation nav = findViewById(R.id.nav_bottom);
        nav.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        nav.setCurrentItem(1);
        nav.setBackgroundResource(android.R.color.white);
        nav.setAccentColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        nav.setOnTabSelectedListener((position, wasSelected) -> {
            switch (position) {
                case Constant.HISTORY_TAB:
                    loadFragment(new HistoryFrag());
                    return true;
                case Constant.HOME_TAB:
                    loadFragment(MenuFrag.newInstance(MenuFrag.HOME));
                    return true;
//                case Constant.TRANSACTION_TAB:
//                    loadFragment(MenuFrag.newInstance(MenuFrag.TRX));
//                    return true;
                case Constant.PROFILE_TAB:
                    loadFragment(new ProfileFrag());
                    return true;
            }
            return true;
        });
        AHBottomNavigationAdapter adapter = new AHBottomNavigationAdapter(this, R.menu.main);
        adapter.setupWithBottomNavigation(nav);

        //Hendric - 20180703 - From Result VA Generated back to tab Pembayaran
        //Hendric - 20180809 - From Bill & QR Payment Generated back to tab History
        switch (getIntent().getIntExtra(Constant.BACK_TO, Constant.HOME_TAB))
        {
//            case Constant.TRANSACTION_TAB:
//                nav.setCurrentItem(Constant.TRANSACTION_TAB);
//                break;
            case Constant.HISTORY_TAB:
                nav.setCurrentItem(Constant.HISTORY_TAB);
                break;
            default:
                nav.setCurrentItem(Constant.HOME_TAB);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TokenizedRequestBody body = new TokenizedRequestBody();
        body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
        APIRequest<ProfileResponseBody> request = new APIRequest<>(this,
                APIRequest.Instance.GET_PROFILE, body, this, this);
        queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        reloadCurrentFragment();
    }

    @Override
    public void onResponse(ProfileResponseBody response) {
        ResponseStatus status = ResponseStatus.getByValue(response.status);

        if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, Object> values = new HashMap<>();
        values.put(Constant.PREF_NAME, response.name);
        values.put(Constant.PREF_PHONE, response.phone);
        values.put(Constant.PREF_BALANCE, response.balance);
        values.put(Constant.PREF_IMAGE , response.img);
        PreferencesUtil.save(this, values);
        reloadCurrentFragment();
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trx = manager.beginTransaction();
        trx.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        Fragment old = manager.findFragmentByTag(FRAGMENT);
        if (old != null) {
            trx.hide(old).remove(old);
        }
        trx.add(R.id.frame_container, fragment, FRAGMENT).show(fragment);
        trx.commit();
    }

    private void reloadCurrentFragment() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(FRAGMENT);
        manager.beginTransaction().detach(fragment).attach(fragment).commit();
    }
}
