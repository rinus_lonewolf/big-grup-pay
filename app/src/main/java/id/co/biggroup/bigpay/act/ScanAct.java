package id.co.biggroup.bigpay.act;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.Objects;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.adapter.SpinnerAdapter;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.QRPayRequestBody;
import id.co.biggroup.bigpay.api.response.QRPayResponseBody;
import id.co.biggroup.bigpay.databinding.ActQrFormBinding;
import id.co.biggroup.bigpay.enums.PayMethod;
import id.co.biggroup.bigpay.form.QRInputForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

public class ScanAct extends NavBarAct implements WidgetEventHandler, Response.Listener<QRPayResponseBody>
{
  private int step;
  private ActQrFormBinding binding;
  private PayMethod[] methods = {PayMethod.WALLET};

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    step = getIntent().getIntExtra(Constant.DATA_STEP, Constant.STEP_SCAN);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
    {
      if (ContextCompat.checkSelfPermission(this,
        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.CAMERA}, Activity.RESULT_FIRST_USER);
      else
        setStep();
    }
    else
      setStep();
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  private void setStep()
  {
    if (step == Constant.STEP_SCAN)
    {
      setContentView(R.layout.act_qr_scan);
      setTitle(R.string.title_qr_scan);
    }
    else
    {
      QRInputForm form = (QRInputForm) getIntent().getSerializableExtra(Constant.DATA_FORM);
      form.setStep(step);
      binding = DataBindingUtil.setContentView(this, R.layout.act_qr_form);
      binding.spinnerPayMethod.setAdapter(new SpinnerAdapter(this, methods));
      binding.setForm(form);
      binding.setHandler(this);

      if (step == Constant.STEP_INPUT)
        setTitle(R.string.title_qr_input);
      else if (step == Constant.STEP_CONFIRM)
        setTitle(R.string.title_qr_confirm);
      else if (step == Constant.STEP_RESULT)
      {
        Objects.requireNonNull(getSupportActionBar()).hide();

        if (form.paymentSuccess) {
          binding.imgPayStatus.setImageDrawable(getDrawable(R.drawable.success));
          binding.textPayStatus.setText(R.string.msg_qr_success);
        }
        else {
          binding.imgPayStatus.setImageDrawable(getDrawable(R.drawable.failed));
          binding.textPayStatus.setText(R.string.msg_qr_failed);
        }
        binding.buttonNext.setText(R.string.label_done);
      }
    }
  }

  @Override
  public void onBackPressed()
  {
    if (step == Constant.STEP_RESULT)
      finish();
    else
      super.onBackPressed();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
  {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == Activity.RESULT_FIRST_USER)
    {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
        setStep();
      else
      {
        Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
        finish();
      }
    }
  }

  @SuppressLint("HardwareIds")
  @Override
  public void onClick(View view)
  {
    int step = getIntent().getIntExtra(Constant.DATA_STEP, Constant.STEP_SCAN);
    if (step == Constant.STEP_INPUT)
    {
      QRInputForm form = binding.getForm();
      form.paymentMethod = form.getPayMethod().getLabel();

      Intent i = new Intent(this, ScanAct.class);
      i.putExtra(Constant.DATA_STEP, step + 1);
      i.putExtra(Constant.DATA_FORM, form);
      startActivity(i);
    }
    else if (step == Constant.STEP_CONFIRM)
    {
      hideInputProgress(false);

      QRPayRequestBody body = new QRPayRequestBody();
      body.ecommRefNo = binding.getForm().ecommRefNo;
      body.userEmailApps = PreferencesUtil.getString(this, Constant.PREF_EMAIL);
      body.customerName = PreferencesUtil.getString(this, Constant.PREF_NAME);
      body.accountToken = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
      body.paymentMethodCode = getString(R.string.app_name);
      body.inputAmount = binding.getForm().amount.toString();
      body.deviceId = Settings.Secure.getString(this.getContentResolver(),
        Settings.Secure.ANDROID_ID);
      body.paymentGatewayId = getString(R.string.pg_id);
//      body.paymentByChannel = Constant.PAYMENT_CHANNEL;
      body.checksum =  AuthUtil.digest(body.flatten() + getString(R.string.pg_key));

      APIRequest<QRPayResponseBody> request = new APIRequest<>(this,
        APIRequest.Instance.TRX_QR_PAY, body, this, this, "QR");
      queue = Volley.newRequestQueue(this);
      queue.add(request);
    }
    else if (step == Constant.STEP_RESULT) {
      Intent i = new Intent(this, MainAct.class);
      i.putExtra(Constant.BACK_TO, Constant.HISTORY_TAB);
      i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(i);
    }
  }

  @Override
  public void onResponse(QRPayResponseBody response)
  {
    if (step == Constant.STEP_CONFIRM)
    {
      hideInputProgress(true);

      QRInputForm form = binding.getForm();
      form.paymentSuccess = response.responseCode.equals("BYR00");

      Intent i = new Intent(this, ScanAct.class);
      i.putExtra(Constant.DATA_STEP, step + 1);
      i.putExtra(Constant.DATA_FORM, form);
      startActivity(i);
    }
  }

  @Override
  public void onErrorResponse(VolleyError error)
  {
    super.onErrorResponse(error);
    hideInputProgress(true);
  }

  private void hideInputProgress(boolean flag) {
    binding.buttonNext.setEnabled(flag);
    if (flag)
      binding.progressBar.setVisibility(View.GONE);
    else
      binding.progressBar.setVisibility(View.VISIBLE);
  }
}
