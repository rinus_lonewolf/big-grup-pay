package id.co.biggroup.bigpay.act;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.SignupRequestBody;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.databinding.ActSignupBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.SignUpForm;
import id.co.biggroup.bigpay.form.SigninForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.ValidationUtil;

public class SignUpAct extends BaseAct implements WidgetEventHandler, Response.Listener<APIResponseBody> {
    private ActSignupBinding binding;
    private ValidationUtil validationUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBarWhite);
        binding = DataBindingUtil.setContentView(this, R.layout.act_signup);
        binding.setForm(new SignUpForm());
        binding.setHandler(this);
        binding.viewText.setMovementMethod(LinkMovementMethod.getInstance());
        validationUtil = new ValidationUtil(this);
    }

    @Override
    public void onClick(View view) {
        if (binding.progressBar.getVisibility() == View.VISIBLE) {
            return;
        }
        if (!validate()) {
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        SignUpForm form = binding.getForm();
        SignupRequestBody body = new SignupRequestBody();
        body.email = form.getEmail();
        body.name = form.getName();
        body.phone = form.getHandphone();
        body.password = AuthUtil.digest(form.getPassword());
        APIRequest<APIResponseBody> request = new APIRequest<>(this,
                APIRequest.Instance.SIGN_UP, body, this, this);
        queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private boolean validate() {
        boolean valid = validationUtil.isFilled(binding.inputName);
        if (!validationUtil.isFilled(binding.inputEmail)) {
            valid &= false;
        } else {
            valid &= validationUtil.isPatternValid(binding.inputEmail, ValidationUtil.EMAIL_PATTERN);
        }
        valid &= validationUtil.isFilled(binding.inputHandphone);
        if (!validationUtil.isFilled(binding.inputPassword)) {
            valid &= false;
        } else if (!validationUtil.isLengthValid(binding.inputPassword, ValidationUtil.LengthType.MIN, Constant.KEY_MIN_LENGTH)) {
            valid &= false;
        } else {
            valid &= validationUtil.isPatternValid(binding.inputPassword, ValidationUtil.PASSWORD_PATTERN);
        }
        valid &= validationUtil.isEqual(binding.inputConfirm, binding.inputPassword);
        valid &= binding.checkboxAgreement.isChecked();

        return valid;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        binding.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(APIResponseBody response) {
        binding.progressBar.setVisibility(View.INVISIBLE);
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, R.string.notif_success_signup, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, VerifyAct.class);
        SigninForm form = new SigninForm();
        form.setEmail(binding.getForm().getEmail());
        form.setKey(binding.getForm().getPassword());
        i.putExtra(Constant.DATA_FORM, form);
        startActivity(i);
        finish();
    }
}
