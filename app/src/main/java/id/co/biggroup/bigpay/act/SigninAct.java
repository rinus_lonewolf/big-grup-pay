package id.co.biggroup.bigpay.act;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.SigninRequestBody;
import id.co.biggroup.bigpay.api.response.SigninResponseBody;
import id.co.biggroup.bigpay.databinding.ActSigninBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.SigninForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;
import id.co.biggroup.bigpay.util.ValidationUtil;

public class SigninAct extends BaseAct implements WidgetEventHandler, Response.Listener<SigninResponseBody> {

    private ActSigninBinding binding;
    private ValidationUtil validationUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBarWhite);
        binding = DataBindingUtil.setContentView(this, R.layout.act_signin);
        binding.setHandler(this);
        SigninForm form = new SigninForm();
        form.setEmail(PreferencesUtil.getString(this, Constant.PREF_EMAIL));
        binding.setForm(form);
        validationUtil = new ValidationUtil(this);
    }

    @Override
    public void onClick(View view) {
        if (binding.progressBar.getVisibility() == View.VISIBLE) {
            return;
        }
        if (view.equals(binding.buttonSignin) && validate()) {
            binding.progressBar.setVisibility(View.VISIBLE);
            SigninForm form = binding.getForm();
            SigninRequestBody body = new SigninRequestBody();
            body.email = form.getEmail();
            body.password = AuthUtil.digest(form.getKey());
            APIRequest<SigninResponseBody> request = new APIRequest<>(this,
                    APIRequest.Instance.SIGN_IN, body, this, this);
            queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else if (view.equals(binding.buttonForgot)) {
            Intent i = new Intent(this, ForgotKeyAct.class);
            startActivity(i);
        }
    }

    private boolean validate() {
        boolean valid = true;
        if (!validationUtil.isFilled(binding.inputId)) {
            valid &= false;
        } else {
            valid &= validationUtil.isPatternValid(binding.inputId, ValidationUtil.EMAIL_PATTERN);
        }
        valid &= validationUtil.isFilled(binding.inputKey);

        return valid;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        binding.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(SigninResponseBody response) {
        binding.progressBar.setVisibility(View.INVISIBLE);
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (ResponseStatus.SIGN_IN_UNVERIFIED.equals(status)) {
            Intent i = new Intent(this, VerifyAct.class);
            i.putExtra(Constant.DATA_FORM, binding.getForm());
            startActivity(i);
            return;
        } else if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, Object> values = new HashMap<>();
        values.put(Constant.PREF_TOKEN, response.token);
        values.put(Constant.PREF_EMAIL, response.email);
        PreferencesUtil.save(this, values);
        Intent i = new Intent(this, MainAct.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}
