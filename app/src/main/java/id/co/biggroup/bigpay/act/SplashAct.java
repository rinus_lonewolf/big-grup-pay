package id.co.biggroup.bigpay.act;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.view.Window;

import org.apache.commons.lang3.StringUtils;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.util.LanguageUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

/**
 * Created by developer on 3/27/18.
 */

public class SplashAct extends AppCompatActivity //implements AsyncResponse
{
  private String currentVersion, onlineVersion;
  private boolean retry = false;

  @SuppressLint("NewApi")
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

    if (isLollipop)
      requestWindowFeature(Window.FEATURE_CONTENT_TRANSITIONS);
    if (isLollipop)
      getWindow().setExitTransition(new Fade());

    try
    {
      currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
      //without check online Google Play Store version
      Handler handler = new Handler();
      handler.postDelayed(task, 2000);
    }
    catch (PackageManager.NameNotFoundException e)
    {
      e.printStackTrace();
    }

    LanguageUtil.checkLanguage(this);
  }

  private Runnable task = () -> {
    String token = PreferencesUtil.getString(SplashAct.this, Constant.PREF_TOKEN);
    Class clazz = MainAct.class;
    if (StringUtils.isBlank(token))
    {
      clazz = InitAct.class;
    }
    Intent intent = new Intent(SplashAct.this, clazz);
    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashAct.this);
    ActivityCompat.startActivity(SplashAct.this, intent, options.toBundle());
    ActivityCompat.finishAfterTransition(SplashAct.this);
  };

  @Override
  protected void onResume()
  {
    super.onResume();
//    new VersionChecker(this).execute();
  }

//  @Override
//  public void processFinish(String output)
//  {
//    if (output != null && !output.isEmpty())
//    {
//      onlineVersion = output;
//      if (output.equals(currentVersion))
//      {
//        Handler handler = new Handler();
//        handler.postDelayed(task, 2000);
//      }
//      else
//        showUpdateDialog();
//    }
//  }

//  @Override
//  public void processError()
//  {
//    if (!retry)
//    {
//      Snackbar.make(getWindow().getDecorView().getRootView(), R.string.info_connection_unstable, Snackbar.LENGTH_INDEFINITE)
//        .setAction(R.string.label_retry, view -> new VersionChecker(this).execute()).show();
//      retry = true;
//    }
//    else
//      Snackbar.make(getWindow().getDecorView().getRootView(), R.string.info_connection_unstable_2, Snackbar.LENGTH_INDEFINITE)
//        .setAction(R.string.btn_signout, view -> finish()).show();
//  }

//  private void showUpdateDialog()
//  {
//    new AlertDialog.Builder(SplashAct.this)
//      .setIcon(R.mipmap.logo_splash)
//      .setCancelable(false)
//      .setTitle(R.string.app_name)
//      .setMessage(getString(R.string.info_update_app, onlineVersion, currentVersion))
//      .setPositiveButton(R.string.label_update, (dialog, which) -> {
//        dialog.dismiss();
//        try
//        {
//          SplashAct.this.startActivity(new Intent(Intent.ACTION_VIEW,
//            Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
//        }
//        catch (android.content.ActivityNotFoundException anfe)
//        {
//          SplashAct.this.startActivity(new Intent(Intent.ACTION_VIEW,
//            Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
//        }
//      }).show();
//  }
}