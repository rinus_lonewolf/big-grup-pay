package id.co.biggroup.bigpay.act;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.databinding.ActTacBinding;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;

public class TacAct extends BaseAct implements WidgetEventHandler{

    private ActTacBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBarWhite);
        setContentView(R.layout.act_tac);
        binding = DataBindingUtil.setContentView(this, R.layout.act_tac);
        binding.viewWeb.loadUrl("file:///android_asset/tac.html");
        binding.setHandler(this);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
