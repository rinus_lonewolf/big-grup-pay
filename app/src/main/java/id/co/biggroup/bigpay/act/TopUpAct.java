package id.co.biggroup.bigpay.act;

import android.os.Bundle;
import android.widget.ExpandableListView;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.adapter.ELVAdapter;
import id.co.biggroup.bigpay.enums.TopupBank;
import id.co.biggroup.bigpay.listener.ELVExpandListener;

public class TopUpAct extends NavBarAct {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_top_up);
        ExpandableListView listView = findViewById(R.id.view_list_expandable);
        listView.setAdapter(new ELVAdapter(TopupBank.values(), this));
        listView.setOnGroupExpandListener(new ELVExpandListener(listView));
    }
}
