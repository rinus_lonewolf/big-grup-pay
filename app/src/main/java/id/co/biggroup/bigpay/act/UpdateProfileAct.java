package id.co.biggroup.bigpay.act;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.UpdateProfileRequestBody;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.api.response.ProfileResponseBody;
import id.co.biggroup.bigpay.databinding.ActUpdateProfileBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.CameraUtil;
import id.co.biggroup.bigpay.util.CommonUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

public class UpdateProfileAct extends NavBarAct implements WidgetEventHandler, Response.Listener<APIResponseBody> {
  private ActUpdateProfileBinding binding;
  private String strImage;
  private File photoFile;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.act_update_profile);
    binding.setHandler(this);
    ProfileResponseBody profile = PreferencesUtil.getProfile(this);
    binding.setProfile(profile);
    if (profile.img != null && !profile.img.equals(""))
      binding.imgProfile.setImageBitmap(CommonUtil.decodeToBase64(profile.img));
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
  {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == Activity.RESULT_FIRST_USER)
    {
      boolean isAllGranted = true;
      for (int grantResult : grantResults)
      {
        if (grantResult == PackageManager.PERMISSION_DENIED)
          isAllGranted = false;
      }
      if (isAllGranted)
        openDialogChooser();
      else
        Toast.makeText(this, R.string.info_failed_camera_storage_access, Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onClick(View view)
  {
    if (view == binding.fabChangePhoto)
    {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      {
        if (ContextCompat.checkSelfPermission(this,
          Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this,
          Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
          ActivityCompat.requestPermissions(this,
            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
            Activity.RESULT_FIRST_USER);
        }
        else
          openDialogChooser();
      }
      else
        openDialogChooser();
    }
    else
    {
      binding.layoutName.setError(null);

      if (binding.inputName.getText() != null && binding.inputName.getText().length() >= 3)
      {
        binding.progressBar.setVisibility(View.VISIBLE);

        UpdateProfileRequestBody body = new UpdateProfileRequestBody();
        body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
        body.name = binding.inputName.getText().toString();
        if (strImage != null && !strImage.equals(""))
          body.image = strImage;
        APIRequest<APIResponseBody> request = new APIRequest<>(this,
          APIRequest.Instance.UPDATE_PROFILE, body, this, this);
        queue = Volley.newRequestQueue(this);
        queue.add(request);
      }
      else
        binding.layoutName.setError(getString(R.string.info_fullname_error));
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      try {
        Uri imageUri;
        InputStream imageStream;
        Bitmap selectedImage = null;
        switch (requestCode) {
          case 0:
            imageUri = Uri.fromFile(photoFile);
            if (imageUri != null)
              selectedImage = CommonUtil.resizeBitmap(CameraUtil.handleSamplingAndRotationBitmap(
                this, imageUri), Constant.PHOTO_MAX_SIZE);
            break;
          case 1:
            imageUri = data.getData();
            if (imageUri != null) {
              imageStream = getContentResolver().openInputStream(imageUri);
              selectedImage = CommonUtil.resizeBitmap(BitmapFactory.decodeStream(imageStream),
                Constant.PHOTO_MAX_SIZE);
            }
            break;
        }
        if (selectedImage != null) {
          strImage = CommonUtil.encodeToBase64(selectedImage);
          binding.imgProfile.setImageBitmap(selectedImage);
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void onResponse(APIResponseBody response)
  {
    binding.progressBar.setVisibility(View.INVISIBLE);
    ResponseStatus status = ResponseStatus.getByValue(response.status);
    if (!Status.SUCCESS.equals(status.getStatus())) {
      Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
    }
    else {
      finish();
    }
  }

  private void openDialogChooser()
  {
    new AlertDialog.Builder(this)
      .setCancelable(false)
      .setNeutralButton(R.string.label_cancel, (dialog, which) -> dialog.dismiss())
      .setPositiveButton(getString(R.string.label_camera), (dialog, which) ->
      {
        Intent photoPickerIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (photoPickerIntent.resolveActivity(getPackageManager()) != null)
        {
          photoFile = null;
          try {
            photoFile = CommonUtil.createImageFile();
          } catch (IOException ex) {
            ex.printStackTrace();
          }
          // Continue only if the File was successfully created
          if (photoFile != null) {
            photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(
              this, getString(R.string.provider_package), photoFile));
            startActivityForResult(photoPickerIntent, 0);
          }
        }
        dialog.dismiss();
      })
      .setNegativeButton(getString(R.string.label_gallery), (dialog, which) ->
      {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
          MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (photoPickerIntent.resolveActivity(getPackageManager()) != null)
          startActivityForResult(photoPickerIntent, 1);
        dialog.dismiss();
      })
      .show();
  }
}
