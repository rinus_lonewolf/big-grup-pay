package id.co.biggroup.bigpay.act;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.adapter.SpinnerAdapter;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.GetVARequestBody;
import id.co.biggroup.bigpay.api.response.GetVAResponseBody;
import id.co.biggroup.bigpay.databinding.ActVaConfirmBinding;
import id.co.biggroup.bigpay.databinding.ActVaInputBinding;
import id.co.biggroup.bigpay.databinding.ActVaResultBinding;
import id.co.biggroup.bigpay.enums.PayMethod;
import id.co.biggroup.bigpay.enums.VAResidential;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.VAForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.model.SpinnerItem;
import id.co.biggroup.bigpay.util.CommonBindingAdapter;
import id.co.biggroup.bigpay.util.PreferencesUtil;

public class VAAct extends NavBarAct implements WidgetEventHandler, CommonBindingAdapter.Listener,
  Response.Listener<GetVAResponseBody>
{

  private ActVaInputBinding inputBinding;
  private ActVaConfirmBinding confirmBinding;
  private int step;
  private String[] historyUnit = new String[1];

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    step = getIntent().getIntExtra(Constant.DATA_STEP, Constant.STEP_INPUT);
    if (step == Constant.STEP_INPUT)
    {
      inputBinding = DataBindingUtil.setContentView(this, R.layout.act_va_input);
      inputBinding.setForm(new VAForm());
      inputBinding.setHandler(this);
      inputBinding.setListener(this);
      inputBinding.spinnerResidentialName.setAdapter(new SpinnerAdapter(this, VAResidential.values()));

      //Hendric - 20180628 -  Add historyUnit adapter to EditText Unit
      if (PreferencesUtil.getString(this,
        Constant.PREFIX_HISTORY_UNIT + PreferencesUtil.getString(this, Constant.PREF_EMAIL)) != null)
      {
        historyUnit[0] = PreferencesUtil.getString(this,
          Constant.PREFIX_HISTORY_UNIT + PreferencesUtil.getString(this, Constant.PREF_EMAIL));
      }
      ArrayAdapter<String> adapter =
        new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, historyUnit);
      inputBinding.inputRegNum.setThreshold(1);
      inputBinding.inputRegNum.setAdapter(adapter);
    }
    else if (step == Constant.STEP_CONFIRM)
    {
      confirmBinding = DataBindingUtil.setContentView(this, R.layout.act_va_confirm);
      VAForm form = (VAForm) getIntent().getSerializableExtra(Constant.DATA_FORM);
      confirmBinding.setForm(form);
      confirmBinding.setHandler(this);
      confirmBinding.setListener(this);
      PayMethod[] methods = {PayMethod.BCA, PayMethod.MANDIRI};
      confirmBinding.spinnerPayMethod.setAdapter(new SpinnerAdapter(this, methods));
      if (form.paymentStatus != 0)
        confirmBinding.button.setText(R.string.label_back);
    }
    else
    {
      ActVaResultBinding binding = DataBindingUtil.setContentView(this, R.layout.act_va_result);
      VAForm form = (VAForm) getIntent().getSerializableExtra(Constant.DATA_FORM);
      binding.setForm(form);
      binding.setHandler(this);
    }
  }

  @Override
  public void onClick(View view)
  {
    if (step == Constant.STEP_INPUT)
    {
      boolean valid = true;
      String msg = getString(R.string.field_required);
      if (inputBinding.inputRegNum.getText().toString().length() == 0)
      {
        inputBinding.inputRegNum.setError(msg);
        valid &= false;
      }
      if (!valid)
      {
        return;
      }
      inputBinding.progressBar.setVisibility(View.VISIBLE);
      VAForm form = inputBinding.getForm();
      GetVARequestBody body = new GetVARequestBody();
      body.regNo = form.getRegNum();
      body.residentialCode = form.getResidentialCode();
      body.token = PreferencesUtil.getString(this, Constant.PREF_TOKEN);
      APIRequest<GetVAResponseBody> request = new APIRequest<>(this,
        APIRequest.Instance.GET_EDU_VA, body, this, this);
      queue = Volley.newRequestQueue(this);
      queue.add(request);
      return;
    }
    if (step == Constant.STEP_CONFIRM)
    {
      if (view == confirmBinding.textMoreDetail)
      {
        if (confirmBinding.layoutDetail.getVisibility() == View.GONE)
        {
          confirmBinding.textMoreDetail.setText(R.string.label_hide_detail);
          confirmBinding.layoutDetail.setVisibility(View.VISIBLE);
        }
        else
        {
          confirmBinding.textMoreDetail.setText(R.string.label_show_detail);
          confirmBinding.layoutDetail.setVisibility(View.GONE);
        }
      }
      else
      {
        VAForm form = confirmBinding.getForm();
        if (form.paymentStatus != 0)
        {
          finish();
          return;
        }
        if (PayMethod.B_PAY.equals(form.getPayMethod()))
        {
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setMessage(R.string.msg_qr_success);
          builder.setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
            Intent intent = new Intent(VAAct.this, MainAct.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
          });
          builder.create().show();
          return;
        }
        if (form.getVaNumber() == null)
        {
          Toast.makeText(this, R.string.notif_failed_va_gen, Toast.LENGTH_SHORT).show();
          return;
        }
        Intent i = new Intent(this, VAAct.class);
        i.putExtra(Constant.DATA_STEP, step + 1);
        i.putExtra(Constant.DATA_FORM, form);
        startActivity(i);
      }
    }
    if (step == Constant.STEP_RESULT)
    {
      Intent i = new Intent(this, MainAct.class);
      i.putExtra(Constant.BACK_TO, Constant.HOME_TAB);
      i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(i);
    }
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
  {
    if (parent.getId() == R.id.spinner_pay_method)
    {
      SpinnerItem item = (SpinnerItem) parent.getAdapter().getItem(position);
      if (PayMethod.B_PAY.equals(item))
        confirmBinding.button.setText(getString(R.string.label_pay));
      else
        confirmBinding.button.setText(getString(R.string.label_generate_va));
    }
  }

  @Override
  public void onErrorResponse(VolleyError error)
  {
    super.onErrorResponse(error);
    inputBinding.progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  public void onResponse(GetVAResponseBody response)
  {
    inputBinding.progressBar.setVisibility(View.INVISIBLE);
    ResponseStatus status = ResponseStatus.getByValue(response.status);
    if (!Status.SUCCESS.equals(status.getStatus()))
    {
      Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
    }
    else
    {
      //Hendric - 20180628 -  Add historyUnit adapter to EditText NIK
      PreferencesUtil.save(this,
        Constant.PREFIX_HISTORY_UNIT + PreferencesUtil.getString(this, Constant.PREF_EMAIL),
        inputBinding.inputRegNum.getText().toString());

//      Log.d("VAAct","response : "+response);
      VAForm form = inputBinding.getForm();
      form.name = response.name;
//      form.monthlyCharge = response.monthlyCharge;
//      form.ppn = response.ppn;
      form.period = response.date;
//      form.watery = response.watery;
//      form.electricity = response.electricity;
//      form.electricSubscription = response.electricSubscription;
//      form.publicLightning = response.publicLightning;
//      form.changingFixing = response.changingFixing;
//      form.administrationFee = response.administrationFee;
//      form.waterMaintenance = response.waterMaintenance;
      form.stamp = response.stamp;
      form.penalty = response.penalty;
      form.fee = response.fee;
      form.payAmount = response.payAmount;
      form.amount = response.amount;
      form.vas = response.vas;
      form.dueDate = response.validity;
      form.paymentStatus = response.paymentStatus;
      form.paidDate = response.paidDate;

      Log.d("VAAct","response.daya_terpasang : "+response.daya_terpasang);
      Log.d("VAAct","response.kwh_listrik_awal : "+response.kwh_listrik_awal);
      Log.d("VAAct","response.kwh_listrik_akhir : "+response.kwh_listrik_akhir);
      Log.d("VAAct","response.meter_awal_air : "+response.meter_awal_air);
      Log.d("VAAct","response.meter_akhir_air : "+response.meter_akhir_air);
      Log.d("VAAct","response.unit : "+response.unit);
      Log.d("VAAct","response.tipe : "+response.tipe);
      Log.d("VAAct","response.luas : "+response.luas);
      Log.d("VAAct","response.iuran_pemeliharaan : "+response.iuran_pemeliharaan);
      Log.d("VAAct","response.ppn_new : "+response.ppn_new);
      Log.d("VAAct","response.iuran_perbaikan_penggantian : "+response.iuran_perbaikan_penggantian);
      Log.d("VAAct","response.biaya_pemeliharaan_listrik : "+response.biaya_pemeliharaan_listrik);
      Log.d("VAAct","response.total_biaya_listrik : "+response.total_biaya_listrik);
      Log.d("VAAct","response.administrasi : "+response.administrasi);
      Log.d("VAAct","response.pju : "+response.pju);
      Log.d("VAAct","response.biaya_meter_air : "+response.biaya_meter_air);
      Log.d("VAAct","response.total_biaya_air : "+response.total_biaya_air);
      Log.d("VAAct","response.materai_new : "+response.materai_new);
      Log.d("VAAct","response.denda : "+response.denda);
      Log.d("VAAct","response.jumlah_tagihan : "+response.jumlah_tagihan);
      Log.d("VAAct","response.biaya_transaksi : "+response.biaya_transaksi);

        form.daya_terpasang = response.daya_terpasang;
        form.kwh_listrik_awal = response.kwh_listrik_awal;
        form.kwh_listrik_akhir = response.kwh_listrik_akhir;
        form.meter_awal_air = response.meter_awal_air;
        form.meter_akhir_air = response.meter_akhir_air;
        form.unit = response.unit;
        form.tipe = response.tipe;
        form.luas = response.luas;
        form.iuran_pemeliharaan = response.iuran_pemeliharaan;
        form.ppn_new = response.ppn_new;
        form.iuran_perbaikan_penggantian = response.iuran_perbaikan_penggantian;
        form.biaya_pemeliharaan_listrik = response.biaya_pemeliharaan_listrik;
        form.total_biaya_listrik = response.total_biaya_listrik;
        form.administrasi = response.administrasi;
        form.pju = response.pju;
        form.biaya_meter_air = response.biaya_meter_air;
        form.total_biaya_air = response.total_biaya_air;
        form.stamp = response.materai_new;
        form.penalty = response.denda;
        form.jumlah_tagihan = response.jumlah_tagihan;
        form.biaya_transaksi = response.biaya_transaksi;

      Intent i = new Intent(this, VAAct.class);
      i.putExtra(Constant.DATA_STEP, step + 1);
      i.putExtra(Constant.DATA_FORM, form);
      startActivity(i);
    }
  }
}
