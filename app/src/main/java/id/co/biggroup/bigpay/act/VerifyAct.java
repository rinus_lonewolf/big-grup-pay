package id.co.biggroup.bigpay.act;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.VerifyRequestBody;
import id.co.biggroup.bigpay.api.response.SigninResponseBody;
import id.co.biggroup.bigpay.databinding.ActVerifyBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.form.SigninForm;
import id.co.biggroup.bigpay.form.VerifyForm;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;
import id.co.biggroup.bigpay.util.ValidationUtil;

public class VerifyAct extends BaseAct implements WidgetEventHandler, Response.Listener<SigninResponseBody> {

    private ActVerifyBinding binding;
    private ValidationUtil validationUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.act_verify);
        binding.setForm(new VerifyForm());
        binding.setHandler(this);
        validationUtil = new ValidationUtil(this);
    }

    @Override
    public void onClick(View view) {
        if (binding.progressBar.getVisibility() == View.VISIBLE) {
            return;
        }
        if (!validationUtil.isFilled(binding.inputCode)) {
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        VerifyRequestBody body = new VerifyRequestBody();
        body.code = binding.getForm().getCode();
        SigninForm form = (SigninForm) getIntent().getSerializableExtra(Constant.DATA_FORM);
        body.email = form.getEmail();
        body.password = AuthUtil.digest(form.getKey());
        APIRequest<SigninResponseBody> request = new APIRequest<>(this,
                APIRequest.Instance.VERIFY, body, this, this);
        queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        binding.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(SigninResponseBody response) {
        binding.progressBar.setVisibility(View.INVISIBLE);
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(this, status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, R.string.notif_success_verify, Toast.LENGTH_SHORT).show();
        Map<String, Object> values = new HashMap<>();
        values.put(Constant.PREF_TOKEN, response.token);
        values.put(Constant.PREF_EMAIL, response.email);
        PreferencesUtil.save(this, values);
        Intent i = new Intent(this, MainAct.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}
