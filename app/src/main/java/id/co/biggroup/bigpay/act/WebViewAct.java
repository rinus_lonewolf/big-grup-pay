package id.co.biggroup.bigpay.act;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.enums.BuildingInfoItem;
import id.co.biggroup.bigpay.enums.HomeMenuItem;

public class WebViewAct extends BaseAct
{

  private WebView webView;
  private ProgressBar progressBar;

  @SuppressLint("SetJavaScriptEnabled")
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.act_web_view);
    webView = findViewById(R.id.web_view);
    webView.clearCache(true);
    webView.clearHistory();
    webView.getSettings().setJavaScriptEnabled(true);
    webView.getSettings().setSupportZoom(true);
    webView.getSettings().setBuiltInZoomControls(true);
    webView.getSettings().setDisplayZoomControls(false);
    webView.setWebChromeClient(new WebChromeClient());
    webView.setWebViewClient(new WebViewClient()
    {
      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon)
      {
        super.onPageStarted(view, url, favicon);
        progressBar.setVisibility(View.VISIBLE);
      }

      @Override
      public void onPageFinished(WebView view, String url)
      {
        super.onPageFinished(view, url);
        progressBar.setVisibility(View.INVISIBLE);
      }
    });

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    if (getIntent().getStringExtra(Constant.DATA_ITEM).equals(Constant.DATA_ITEM_TYPE_HOME))
    {
      HomeMenuItem item = (HomeMenuItem) getIntent().getSerializableExtra(Constant.DATA_MENU_ITEM);
      if (item.getUrl() != null)
        webView.loadUrl(item.getUrl());

      if (getSupportActionBar() != null)
        getSupportActionBar().setTitle(item.getLabelResourceId());
    }
    else if (getIntent().getStringExtra(Constant.DATA_ITEM).equals(Constant.DATA_ITEM_TYPE_BUILDING))
    {
      BuildingInfoItem item = (BuildingInfoItem) getIntent().getSerializableExtra(Constant.DATA_MENU_ITEM);
      if (item.getUrl() != null)
        webView.loadUrl(item.getUrl());

      if (getSupportActionBar() != null)
        getSupportActionBar().setTitle(item.getLabelResourceId());
    }

    if (getSupportActionBar() != null)
    {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    progressBar = toolbar.findViewById(R.id.progress_bar);
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  protected void onStop()
  {
    super.onStop();
    webView.stopLoading();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    if (item.getItemId() == android.R.id.home)
    {
      finish();
    }
    if (item.getItemId() == R.id.reload)
    {
      webView.stopLoading();
      webView.reload();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    getMenuInflater().inflate(R.menu.browser, menu);
    Drawable wrapper = DrawableCompat.wrap(menu.getItem(0).getIcon());
    DrawableCompat.setTint(wrapper, getResources().getColor(android.R.color.white));

    return super.onCreateOptionsMenu(menu);
  }
}
