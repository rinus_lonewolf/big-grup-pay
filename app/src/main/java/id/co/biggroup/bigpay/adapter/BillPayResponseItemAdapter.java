package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;

import id.co.biggroup.bigpay.R;

public class BillPayResponseItemAdapter extends BaseAdapter
{
  private HashMap<String, String> items;
  private Context context;

  public BillPayResponseItemAdapter(Context context, HashMap<String, String> items)
  {
    this.context = context;
    this.items = items;
  }

  @Override
  public int getCount()
  {
    return items.size();
  }

  @Override
  public Object getItem(int i)
  {
    return items.keySet().toArray()[i].toString();
  }

  @Override
  public long getItemId(int i)
  {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup)
  {
    if (view == null)
      view = LayoutInflater.from(context).inflate(R.layout.item_detail, viewGroup, false);

    TextView tv = view.findViewById(R.id.tv_label);

    String key = items.keySet().toArray()[i].toString();
    tv.setText(key);

    tv = view.findViewById(R.id.tv_value);
    tv.setText(items.get(key));

    return view;
  }

}
