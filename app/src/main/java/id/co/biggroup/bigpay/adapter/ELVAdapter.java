package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.enums.TopupBank;
import id.co.biggroup.bigpay.enums.TopupChannel;
import id.co.biggroup.bigpay.listener.ELVExpandListener;
import id.co.biggroup.bigpay.util.PreferencesUtil;
import id.co.biggroup.bigpay.view.MasterELV;

/**
 * Created by developer on 4/6/18.
 */

public class ELVAdapter extends BaseExpandableListAdapter {
    private final static String PH_VA = "_va_";

    private List<Object> items;
    private Resources resources;
    private LayoutInflater inflater;
    private boolean leaf = false;
    private int len_8dp;
    private int len_16dp;
    private int len_24dp;
    private String bin;
    private String phone;

    public ELVAdapter(Object[] items, Context context) {
        this.items = Arrays.asList(items);
        this.resources = context.getResources();
        this.inflater = LayoutInflater.from(context);

        init();
    }

    public ELVAdapter(TopupBank bank, Context context) {
        this.items = Arrays.asList((Object[]) bank.getChannels());
        this.resources = context.getResources();
        this.inflater = LayoutInflater.from(context);
        this.bin = bank.getBin();
        this.phone = PreferencesUtil.getString(context, Constant.PREF_PHONE);

        init();
    }

    private void init() {
        if (!items.isEmpty() && items.get(0) instanceof TopupChannel) {
            leaf = true;
        }
        len_8dp = resources.getDimensionPixelSize(R.dimen.len_8dp);
        len_16dp = resources.getDimensionPixelSize(R.dimen.len_16dp);
        len_24dp = resources.getDimensionPixelSize(R.dimen.len_24dp);
    }

    @Override
    public int getGroupCount() {
        return items.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (leaf) {
            TopupChannel channel = (TopupChannel) items.get(groupPosition);
            return resources.getStringArray(channel.getSteps()).length;
        }
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return items.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (leaf) {
            TopupChannel channel = (TopupChannel) items.get(groupPosition);
            return resources.getStringArray(channel.getSteps())[childPosition];
        }
        return items.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return items.get(groupPosition).hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.elv_item, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.view_text);
        if (leaf) {
            TopupChannel channel = (TopupChannel) getGroup(groupPosition);
            textView.setText(channel.getLabel());
            convertView.setPadding(len_16dp, len_8dp, len_8dp, len_8dp);
        } else {
            TopupBank bank = (TopupBank) getGroup(groupPosition);
            textView.setText(bank.getLabel());
            convertView.setPadding(len_8dp, len_8dp, len_8dp, len_8dp);
        }
        ImageView imageView = convertView.findViewById(R.id.view_image);
        if (isExpanded) {
            imageView.setImageResource(R.drawable.ic_arrow_up);
        } else {
            imageView.setImageResource(R.drawable.ic_arrow_down);
        }
        convertView.findViewById(R.id.view_number).setVisibility(View.GONE);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (leaf) {
            convertView = inflater.inflate(R.layout.elv_item, parent, false);
        } else {
            MasterELV listView = new MasterELV(inflater.getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            listView.setLayoutParams(params);
            TopupBank bank = (TopupBank) getGroup(groupPosition);
            listView.setAdapter(new ELVAdapter(bank, inflater.getContext()));
            listView.setGroupIndicator(null);
            listView.setOnGroupExpandListener(new ELVExpandListener(listView));
            convertView = listView;
        }
        if (leaf) {
            // guide text
            TextView textView = convertView.findViewById(R.id.view_text);
            String content = (String) getChild(groupPosition, childPosition);
            int index = content.indexOf('\n');
            if (index > -1) {
                index++;
                String substr = content.substring(index);

                if (substr.contains(PH_VA)) {
                    content = content.replace(PH_VA, buildVA(bin, phone));
                }
                SpannableString spannableString = new SpannableString(content);
                spannableString.setSpan(new StyleSpan(Typeface.BOLD), index, content.length(), 0);
                textView.setText(spannableString);
            } else {
                textView.setText(content);
            }
            // guide number
            textView = convertView.findViewById(R.id.view_number);
            textView.setText(String.valueOf(childPosition + 1));
            // others
            convertView.findViewById(R.id.view_image).setVisibility(View.GONE);
            convertView.setPadding(len_24dp, len_8dp, len_8dp, len_8dp);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private String buildVA(String bin, String phone) {
        if (bin == null || phone == null) {
            return null;
        }
        int diff = phone.length() - 11;
        String _phone;

        if (diff > 0) {
            _phone = phone.substring(diff);
        } else {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < -diff; i++) {
                builder.append("0");
            }
            builder.append(phone);
            _phone = builder.toString();
        }
        char[] eh = (bin + _phone).toCharArray();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < eh.length; i++) {
            builder.append(eh[i]);
            if (i + 1 < eh.length && (i + 1) % 4 == 0) {
                builder.append(" ");
            }
        }
        return builder.toString();
    }
}
