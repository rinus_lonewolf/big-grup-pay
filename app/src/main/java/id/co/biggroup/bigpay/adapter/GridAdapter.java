package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 2/21/18.
 */

public class GridAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Pair<Integer, Integer>[] items;

    public GridAdapter(Context context, Pair<Integer, Integer>[] items) {
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return items[i].hashCode();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_grid, null);
        } else {
            view = convertView;
        }
        Pair<Integer, Integer> item = items[i];
        TextView label = view.findViewById(R.id.textView);
        label.setText(item.first);
        ImageView icon = view.findViewById(R.id.imageView);
        icon.setImageResource(item.second);

        return view;
    }
}
