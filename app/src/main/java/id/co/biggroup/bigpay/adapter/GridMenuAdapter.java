package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.model.GridMenuItem;

/**
 * Created by developer on 3/22/18.
 */

public class GridMenuAdapter extends BaseAdapter {

    private List<GridMenuItem> items;
    private LayoutInflater inflater;

    public GridMenuAdapter(Context context, GridMenuItem[] items) {
        this.items = Arrays.asList(items);
        this.inflater = LayoutInflater.from(context);
    }

    public GridMenuAdapter(Context context, List<GridMenuItem> items) {
        this.items = items;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_grid, parent, false);
        } else {
            view = convertView;
        }
        GridMenuItem item = items.get(i);
        TextView label = view.findViewById(R.id.textView);
        label.setText(item.getLabelResourceId());
        ImageView icon = view.findViewById(R.id.imageView);
        icon.setImageResource(item.getIconResourceId());

        return view;
    }
}
