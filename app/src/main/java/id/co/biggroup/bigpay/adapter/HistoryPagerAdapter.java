package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.fragment.BPHistoryFrag;
import id.co.biggroup.bigpay.fragment.VAHistoryFrag;

public class HistoryPagerAdapter extends FragmentPagerAdapter
{
  private Context context;
  private List<Fragment> fragments = new ArrayList<>();

  public HistoryPagerAdapter(Context context, FragmentManager fm)
  {
    super(fm);
    this.context = context;
    fragments.add(new VAHistoryFrag());
    fragments.add(new BPHistoryFrag());
  }

  @Override
  public Fragment getItem(int position)
  {
    return fragments.get(position);
  }

  @Override
  public int getCount()
  {
    return 2;
  }

  @Override
  public CharSequence getPageTitle(int position)
  {
    switch (position)
    {
      case 0:
        return context.getString(R.string.title_va_history);
      case 1:
        return context.getString(R.string.title_bp_history);
    }
    return null;
  }
}
