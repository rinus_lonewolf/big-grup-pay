package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 3/15/18.
 */

public class SpinnerAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final List<SpinnerItem> items;

    public SpinnerAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.items = new ArrayList<>();
    }

    public SpinnerAdapter(Context context, SpinnerItem[] items) {
        this.inflater = LayoutInflater.from(context);
        this.items = new ArrayList<>(Arrays.asList(items));
    }

    public SpinnerAdapter(Context context, List<SpinnerItem> items) {
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    public void clear() {
        this.items.clear();
    }

    public void add(SpinnerItem item) {
        this.items.add(item);
    }

    public void addAll(SpinnerItem[] items) {
        this.items.addAll(Arrays.asList(items));
    }

    public void addAll(Collection<SpinnerItem> items) {
        this.items.addAll(items);
    }

    public int getPosition(SpinnerItem item) {
        return this.items.indexOf(item);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent, true);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent, false);
    }

    private View createItemView(int position, View convertView, ViewGroup parent, boolean isMenu) {
        View view;
        if (convertView == null) {
            int layout = android.R.layout.simple_spinner_dropdown_item;
            if (isMenu) {
                layout = android.R.layout.simple_spinner_item;
            }
            view = inflater.inflate(layout, parent, false);
        } else {
            view = convertView;
        }
        TextView textView = view.findViewById(android.R.id.text1);
        SpinnerItem item = items.get(position);
        textView.setText(item.getLabel());

        return view;
    }
}
