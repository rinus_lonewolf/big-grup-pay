package id.co.biggroup.bigpay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.api.response.VATrx;
import id.co.biggroup.bigpay.util.CommonUtil;

/**
 * Created by developer on 2/21/18.
 */

public class VAHistoryAdapter extends BaseAdapter {
    private final static int VIEW_ITEM = 0;
    private final static int VIEW_FOOTER = 1;
    private List<VATrx> items;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private LayoutInflater inflater;
    private int totalPage;
    private View footer;
    private boolean loading;

    public VAHistoryAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        items = new ArrayList<>();
    }

    public void add(VATrx[] items) {
        if (items == null || items.length == 0) {
            return;
        }
        this.items.addAll(Arrays.asList(items));
    }

    public void clear() {
        items.clear();
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        if (totalPage < 0) {
            return;
        }
        this.totalPage = totalPage;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        if (footer == null) {
            return;
        }
        int visibility = loading ? View.VISIBLE : View.GONE;
        footer.findViewById(R.id.progress_bar).setVisibility(visibility);
    }

    public boolean isLoading() {
        return loading;
    }

    @Override
    public int getCount() {
        return items.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return getItemViewType(i) == VIEW_ITEM ? items.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return getItemViewType(i) == VIEW_ITEM ? getItem(i).hashCode() : -1;
    }

    @Override
    public int getItemViewType(int position) {
        return position >= items.size() ? VIEW_FOOTER : VIEW_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) == VIEW_ITEM;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (getItemViewType(i) == VIEW_FOOTER) {
            return getFooterView(convertView, viewGroup);
        }
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_history, null);
        } else {
            view = convertView;
        }
        VATrx item = items.get(i);
        TextView textView = view.findViewById(R.id.description);
        textView.setText(item.description);
        textView = view.findViewById(R.id.money);
        textView.setText(CommonUtil.formatCurrency(item.amount));
        textView = view.findViewById(R.id.date);
        textView.setText(dateFormat.format(item.paidDate));
        textView = view.findViewById(R.id.status);
        textView.setText(timeFormat.format(item.paidDate));

        return view;
    }

    private View getFooterView(View convertView, ViewGroup parent) {
        if (footer == null) {
            footer = inflater.inflate(R.layout.item_footer, parent, false);
        }
        if (convertView == null) {
            return footer;
        }
        return convertView;
    }
}
