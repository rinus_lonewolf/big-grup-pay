package id.co.biggroup.bigpay.api.request;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.act.InitAct;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.api.response.BPReportResponseBody;
import id.co.biggroup.bigpay.api.response.BalanceResponseBody;
import id.co.biggroup.bigpay.api.response.GetBPResponseBody;
import id.co.biggroup.bigpay.api.response.GetVAResponseBody;
import id.co.biggroup.bigpay.api.response.ProfileResponseBody;
import id.co.biggroup.bigpay.api.response.QRPayResponseBody;
import id.co.biggroup.bigpay.api.response.SigninResponseBody;
import id.co.biggroup.bigpay.api.response.VAReportResponseBody;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

/**
 * Created by developer on 4/24/18.
 */

public class APIRequest<T extends APIResponseBody> extends Request<T> {
    private final static String TAG = APIRequest.class.getName();
    private final static Gson gson = new Gson();

    public enum Instance {
        SIGN_IN(Method.POST, "/bigLoginService", SigninResponseBody.class),
        SIGN_IN_EX(Method.POST, "/bigLoginServiceMk2", SigninResponseBody.class),
        VERIFY(Method.POST, "/bigVerifyUser", SigninResponseBody.class),
        SIGN_OUT(Method.POST, "/bigLogoutService", APIResponseBody.class),
        SIGN_UP(Method.POST, "/bigRegisterUser", APIResponseBody.class),
        SIGN_UP_EX(Method.POST, "/bigRegisterUserMk2", APIResponseBody.class),
        RESET_KEY(Method.POST, "/bigForgotPass", APIResponseBody.class),
        CHANGE_KEY(Method.POST, "/bigChangePass", APIResponseBody.class),
        UPDATE_PROFILE(Method.POST, "/bigUpdateImg", APIResponseBody.class),
        GET_BALANCE(Method.POST, "/bigGetBalance", BalanceResponseBody.class),
        GET_PROFILE(Method.POST, "/bigGetProfile", ProfileResponseBody.class),
        GET_EDU_VA(Method.POST, "/bigGetVA", GetVAResponseBody.class),
        GET_DENOM_PAYOUT(Method.POST, "/bigDenom", GetBPResponseBody.class),
        TRX_BILL_PAY(Method.POST, "/bigTrans", GetBPResponseBody.class),
        TRX_VA_REPORT(Method.POST, "/bigGetReport", VAReportResponseBody.class),
        TRX_BP_REPORT(Method.POST, "/bigBPReport", BPReportResponseBody.class),
        TRX_QR_DATA(Method.POST, "/getQrCodeData", QRPayResponseBody.class),
        TRX_QR_PAY(Method.POST, "/payment", QRPayResponseBody.class)
        ;

        private final int method;
        private final String endpoint;
        private final Class clazz;

        Instance(int method, String endpoint, Class clazz) {
            this.method = method;
            this.endpoint = endpoint;
            this.clazz = clazz;
        }
    }

    private APIRequestBody body;
    private Class<T> clazz;
    private Response.Listener<T> listener;
    private Context context;
    private boolean debug;

    public APIRequest(Context context, Instance instance, APIRequestBody body,
                      Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(instance.method, context.getString(R.string.api_url) + instance.endpoint, errorListener);
        this.body = body;
        this.clazz = instance.clazz;
        this.listener = listener;
        this.context = context;
        this.debug = context.getResources().getBoolean(R.bool.volley_debug);
        //Hendric - 20180927 - Add zero retry policy and 30s timeout
        this.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public APIRequest(Context context, Instance instance, APIRequestBody body,
                      Response.Listener<T> listener, Response.ErrorListener errorListener, String desc) {
        super(instance.method, context.getString(R.string.api_url_qrpay_prod) + instance.endpoint, errorListener);
        this.body = body;
        this.clazz = instance.clazz;
        this.listener = listener;
        this.context = context;
        this.debug = context.getResources().getBoolean(R.bool.volley_debug);
        //Hendric - 20181048 - Add zero retry policy and 30s timeout
        this.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");

        return headers;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            if (debug) {
                Log.d(TAG, json);
            }
            T body = gson.fromJson(json, clazz);

            return Response.success(body, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        //Hendric - 20180718 - Redirect to Login screen if token invalid
        if (response != null) {
            ResponseStatus status = ResponseStatus.getByValue(response.status);
            if (Status.INVALID_TOKEN.equals(status.getStatus())) {
                new AlertDialog.Builder(context)
                  .setIcon(R.drawable.ic_big_group)
                  .setCancelable(false)
                  .setTitle(status.getMessageId())
                  .setMessage(R.string.notif_please_relogin)
                  .setPositiveButton(R.string.label_ok, (dialog, which) -> {
                      dialog.dismiss();
                      PreferencesUtil.removeItems(context,
                        new String[]{Constant.PREF_TOKEN, Constant.PREF_NAME, Constant.PREF_BALANCE});
                      Intent i = new Intent(context, InitAct.class);
                      i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                      context.startActivity(i);
                  }).show();
            }
            else
                listener.onResponse(response);
        } else
            Toast.makeText(context, R.string.notif_other_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getBodyContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (body == null) {
            return null;
        }
        try {
            body.checksum = AuthUtil.digest(body.flatten() + context.getString(R.string.secret_key));;
            String json = gson.toJson(body);
            if (debug) {
                Log.d(TAG, json);
            }
            return json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            VolleyLog.wtf(e, "Unsupported encoding while converting json body to bytes");
        }
        return null;
    }
}
