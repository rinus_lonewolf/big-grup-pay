package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/24/18.
 */

public abstract class APIRequestBody {
    public String checksum;

    public abstract String flatten();
}
