package id.co.biggroup.bigpay.api.request;

public class BillPayRequestBody extends TokenizedRequestBody
{
  public String code;
  public String institutionCode;
  public String billNo;
  public Long amount;
  public String refNo;

  @Override
  public String flatten() {
    return super.flatten() + code + institutionCode + billNo + amount;  //+ refNo + alias + category;
  }
}
