package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/25/18.
 */

public class ChangeKeyRequestBody extends TokenizedRequestBody {
    public String oldPassword;
    public String newPassword;

    @Override
    public String flatten() {
        return token + oldPassword + newPassword;
    }
}
