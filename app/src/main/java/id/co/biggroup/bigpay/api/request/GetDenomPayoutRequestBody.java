package id.co.biggroup.bigpay.api.request;

public class GetDenomPayoutRequestBody extends TokenizedRequestBody
{
  public String institutionCode;
  public Long denom;

  @Override
  public String flatten() {
    return super.flatten() + institutionCode + denom;  //+ refNo + alias + category;
  }
}
