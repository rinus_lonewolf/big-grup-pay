package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 5/2/18.
 */

public class GetVARequestBody extends TokenizedRequestBody {
    public String regNo;
    public String residentialCode;

    @Override
    public String flatten() {
        return super.flatten() + regNo + residentialCode;
    }
}
