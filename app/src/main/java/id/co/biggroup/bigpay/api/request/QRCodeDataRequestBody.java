package id.co.biggroup.bigpay.api.request;

public class QRCodeDataRequestBody extends APIRequestBody
{
  public String qrCodeString;
  public String userEmailApps;
  public String customerName;
  public String deviceId;
  public String paymentGatewayId;

  @Override
  public String flatten()
  {
    return qrCodeString + userEmailApps + customerName + deviceId + paymentGatewayId;
  }
}
