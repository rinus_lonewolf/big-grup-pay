package id.co.biggroup.bigpay.api.request;

public class QRPayRequestBody extends APIRequestBody
{
  public String ecommRefNo;
  public String userEmailApps;
  public String customerName;
  public String accountToken;
  public String paymentMethodCode;
  public String inputAmount;
  public String deviceId;
  public String paymentGatewayId;
//  public int paymentByChannel;
  public String custTokenFcm;

  @Override
  public String flatten()
  {
    return ecommRefNo + userEmailApps + customerName + paymentMethodCode + inputAmount + deviceId + paymentGatewayId;
  }
}
