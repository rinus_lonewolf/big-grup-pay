package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 5/8/18.
 */

public class ReportRequestBody extends TokenizedRequestBody {
    public Integer page;
    public Long startDate;
    public Long endDate;

    @Override
    public String flatten() {
        return super.flatten() + page;
    }
}
