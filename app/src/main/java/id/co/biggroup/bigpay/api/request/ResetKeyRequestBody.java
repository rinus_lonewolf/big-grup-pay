package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 5/16/18.
 */

public class ResetKeyRequestBody extends APIRequestBody {
    public String email;

    @Override
    public String flatten() {
        return email;
    }
}
