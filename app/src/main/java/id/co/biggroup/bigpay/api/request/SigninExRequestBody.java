package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/24/18.
 */

public class SigninExRequestBody extends APIRequestBody {
    public String email;
    public String source;

    @Override
    public String flatten() {
        return email + source;
    }
}
