package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/24/18.
 */

public class SigninRequestBody extends APIRequestBody {
    public String email;
    public String password;

    @Override
    public String flatten() {
        return email + password;
    }
}
