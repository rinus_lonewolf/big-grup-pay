package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/25/18.
 */

public class SignoutRequestBody extends TokenizedRequestBody {
    @Override
    public String flatten() {
        return token;
    }
}
