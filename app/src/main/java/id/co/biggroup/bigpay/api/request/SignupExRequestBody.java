package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/25/18.
 */

public class SignupExRequestBody extends APIRequestBody {
    public String email;
    public String name;
    public String phone;
    public String source;

    @Override
    public String flatten() {
        return email + name + phone + source;
    }
}
