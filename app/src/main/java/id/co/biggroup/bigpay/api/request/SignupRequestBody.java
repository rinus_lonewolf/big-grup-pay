package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/25/18.
 */

public class SignupRequestBody extends APIRequestBody {
    public String email;
    public String name;
    public String phone;
    public String password;

    @Override
    public String flatten() {
        return email + name + phone + password;
    }
}
