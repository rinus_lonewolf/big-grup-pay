package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/25/18.
 */

public class TokenizedRequestBody extends APIRequestBody {
    public String token;

    @Override
    public String flatten() {
        return token;
    }
}
