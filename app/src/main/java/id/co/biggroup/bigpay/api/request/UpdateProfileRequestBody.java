package id.co.biggroup.bigpay.api.request;

public class UpdateProfileRequestBody extends TokenizedRequestBody
{
  public String name;
  public String image;
  public String ext;

  @Override
  public String flatten() {
    return token + name + image + ext;
  }
}
