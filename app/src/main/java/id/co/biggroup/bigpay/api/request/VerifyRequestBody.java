package id.co.biggroup.bigpay.api.request;

/**
 * Created by developer on 4/25/18.
 */

public class VerifyRequestBody extends SigninRequestBody {
    public String code;

    @Override
    public String flatten() {
        return super.flatten() + code;
    }
}
