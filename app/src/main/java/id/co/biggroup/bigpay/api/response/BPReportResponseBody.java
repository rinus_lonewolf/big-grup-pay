package id.co.biggroup.bigpay.api.response;

public class BPReportResponseBody extends APIResponseBody {
  public String name;
  public BPTrx[] items;
  public Integer totalPage;
}
