package id.co.biggroup.bigpay.api.response;

import android.graphics.Color;

import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.util.Date;

import id.co.biggroup.bigpay.R;

@JsonAdapter(BPTrxDeserializer.class)
public class BPTrx implements Serializable {
  public Date date;
  public String biller;
  public String billno;
  public String type;
  public Long amount;
  public String status;
  public String serial;

  public String getType() {
    String desc = "";
    if (type.equals("Db"))
      desc = "Debit";
    else if (type.equals("Cr"))
      desc = "Kredit";
    return desc;
  }

  public String getStatus() {
    String stat;
    if (status == null)
      stat = "Berhasil";
    else {
      if (status.trim().equals("MW - 68") || status.trim().equals("null"))
        stat = "Pending";
      else if (status.trim().substring(0, 2).equals("MW"))
        stat = "Gagal";
      else
        stat = "Berhasil";
    }
    return stat;
  }

  public int getTypeColor() {
    int color = Color.BLACK;
      if (type.equals("Db"))
        color = R.color.colorPrimaryDark;
      else if (type.equals("Cr"))
        color = R.color.red;
    return color;
  }

  public boolean isTokenOrShortRef() {
    return !serial.contains("_") && !serial.trim().equals("");
  }
}
