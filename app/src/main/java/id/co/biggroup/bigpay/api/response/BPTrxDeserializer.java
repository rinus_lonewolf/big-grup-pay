package id.co.biggroup.bigpay.api.response;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

public class BPTrxDeserializer implements JsonDeserializer<BPTrx> {
  private final static String TAG = BPTrxDeserializer.class.getName();

  @Override
  public BPTrx deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
    JsonObject object = json.getAsJsonObject();
    BPTrx bpTrx = new BPTrx();
    if (object.get("billno") != null)
      bpTrx.billno = object.get("billno").getAsString();
    bpTrx.biller = object.get("biller").getAsString();
    bpTrx.type = object.get("type").getAsString();
    bpTrx.amount = object.get("amount").getAsLong();
    bpTrx.serial = object.get("serial").getAsString();
    if (object.get("status") != null)
      bpTrx.status = object.get("status").getAsString();
    try {
      bpTrx.date = new Date(object.get("date").getAsLong());
    } catch (Exception e) {
      Log.e(TAG, "Unable to parse date string", e);
    }
    return bpTrx;
  }
}
