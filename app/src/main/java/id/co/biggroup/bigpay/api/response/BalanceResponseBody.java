package id.co.biggroup.bigpay.api.response;

/**
 * Created by Hendric 07/23/18.
 */

public class BalanceResponseBody extends APIResponseBody {
  public Long balance;
  public Long commision;
}
