package id.co.biggroup.bigpay.api.response;

import com.google.gson.JsonObject;

public class GetBPResponseBody extends APIResponseBody
{
  public JsonObject items;
  public Long payAmount;
  public Long feeAmount;
}
