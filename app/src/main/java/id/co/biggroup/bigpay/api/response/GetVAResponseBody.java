package id.co.biggroup.bigpay.api.response;

import java.math.BigDecimal;

/**
 * Created by developer on 5/2/18.
 */

public class GetVAResponseBody extends APIResponseBody {
    public String email;
    public String name;
    public String phone;
    public String regNo;
    public String residentialCode;
    public Long monthlyCharge;
    public Long ppn;
    public Long watery;
    public Long electricity;
    public Long electricSubscription;
    public BigDecimal publicLightning;
    public Long changingFixing;
    public Long administrationFee;
    public Long waterMaintenance;
    public Long stamp;
    public Long penalty;
    public Long fee;
    public BigDecimal payAmount;
    public VA[] vas;
    public Long amount;
    public String date;
    public String validity;
    public Integer paymentStatus;
    public String paidDate;

    public String daya_terpasang;
    public String kwh_listrik_awal;
    public String kwh_listrik_akhir;
    public String meter_awal_air;
    public String meter_akhir_air;
    public String unit;
    public String tipe;
    public String luas;
    public BigDecimal iuran_pemeliharaan;
    public BigDecimal ppn_new;
    public BigDecimal iuran_perbaikan_penggantian;
    public BigDecimal biaya_pemeliharaan_listrik;
    public BigDecimal total_biaya_listrik;
    public BigDecimal administrasi;
    public BigDecimal pju;
    public BigDecimal biaya_meter_air;
    public BigDecimal total_biaya_air;
    public Long materai_new;
    public Long denda;
    public BigDecimal jumlah_tagihan;
    public BigDecimal biaya_transaksi;
}
