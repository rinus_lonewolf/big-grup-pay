package id.co.biggroup.bigpay.api.response;

/**
 * Created by developer on 4/26/18.
 */

public class ProfileResponseBody extends APIResponseBody {
    public String email;
    public String name;
    public String phone;
    public Long balance;
    public Long commision;
    public String img;
}
