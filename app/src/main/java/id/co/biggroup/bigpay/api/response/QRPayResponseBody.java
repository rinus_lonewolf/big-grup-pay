package id.co.biggroup.bigpay.api.response;

public class QRPayResponseBody extends APIResponseBody
{
  public String responseMessage;
  public String responseCode;
  public String shippingDetails;
  public String remark;
  public String productDetails;
  public String checkoutAmount;
  public String ecommRefNo;
  public String merchantName;
  public String shippingCost;
  public String checkoutCurrency;
  public String merchantRefNo;
  public String checkoutDate;
  public String discount;
}
