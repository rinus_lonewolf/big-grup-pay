package id.co.biggroup.bigpay.api.response;

/**
 * Created by developer on 5/8/18.
 */

public class VAReportResponseBody extends APIResponseBody {
    public String name;
    public VATrx[] items;
    public Integer totalPage;
}
