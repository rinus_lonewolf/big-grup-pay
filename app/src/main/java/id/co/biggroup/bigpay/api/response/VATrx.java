package id.co.biggroup.bigpay.api.response;

import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by developer on 5/8/18.
 */

@JsonAdapter(VATrxDeserializer.class)
public class VATrx implements Serializable {
    public Date createdDate;
    public String description;
    public String regNo;
    public String serviceCode;
    public Date paidDate;
    public Long amount;
}
