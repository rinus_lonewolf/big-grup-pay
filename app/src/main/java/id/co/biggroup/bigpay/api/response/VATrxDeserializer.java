package id.co.biggroup.bigpay.api.response;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by developer on 5/8/18.
 */

public class VATrxDeserializer implements JsonDeserializer<VATrx> {
    private final static DateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    private final static String TAG = VATrxDeserializer.class.getName();

    @Override
    public VATrx deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        VATrx VATrx = new VATrx();
        VATrx.description = object.get("description").getAsString();
        VATrx.regNo = object.get("regNo").getAsString();
        VATrx.serviceCode = object.get("serviceCode").getAsString();
        VATrx.amount = object.get("amount").getAsLong();
        try {
            VATrx.createdDate = format.parse(object.get("createdDate").getAsString());
            VATrx.paidDate = format.parse(object.get("paidDate").getAsString());
        } catch (ParseException e) {
            Log.e(TAG, "Unable to parse date string", e);
        }
        return VATrx;
    }
}
