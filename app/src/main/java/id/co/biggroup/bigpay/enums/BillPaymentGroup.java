package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.model.GridMenuItem;

/**
 * Created by developer on 3/15/18.
 */

public enum BillPaymentGroup implements GridMenuItem {
    RESIDENCE_BILL(R.string.title_bp_resident, R.drawable.ic_bp_resident,
            new BillPaymentType[] { BillPaymentType.PSTN, BillPaymentType.POSTPAID_ELECTRIC, BillPaymentType.TAP_WATER }),
    INSURANCE(R.string.title_bp_insurance, R.drawable.ic_bp_insurance,
            new BillPaymentType[] { BillPaymentType.NATIONAL_INSURANCE, BillPaymentType.PERSONAL_INSURANCE }),
    ENTERTAINMENT(R.string.title_bp_entmt, R.drawable.ic_bp_entmt,
            new BillPaymentType[] { BillPaymentType.TV_CABLE, BillPaymentType.INTERNET }),
    TRAVEL(R.string.title_bp_transport, R.drawable.ic_bp_transport,
            new BillPaymentType[] { BillPaymentType.TRAVEL_TICKET }),
    CREDIT_CARD(R.string.title_bp_cc, R.drawable.ic_bp_cc,
            new BillPaymentType[] { BillPaymentType.CREDIT_CARD }),
    LOAN(R.string.title_bp_loan, R.drawable.ic_bp_loan,
            new BillPaymentType[] { BillPaymentType.LOAN }),
    PREPAID_CELLPHONE(R.string.title_bp_prepaid, R.drawable.ic_bp_prepaid,
            new BillPaymentType[] { BillPaymentType.PREPAID_CELLPHONE }),
    POSTPAID_CELLPHONE(R.string.title_bp_postpaid, R.drawable.ic_bp_postpaid,
            new BillPaymentType[] { BillPaymentType.POSTPAID_CELLPHONE }),
    PREPAID_ELECTRIC(R.string.title_bp_pln_pre, R.drawable.ic_bp_pln_pre,
            new BillPaymentType[] { BillPaymentType.PREPAID_ELECTRIC }),
    ;

    private final int labelId;
    private final int iconId;
    private final BillPaymentType[] types;

    BillPaymentGroup(int labelId, int iconId, BillPaymentType[] types) {
        this.labelId = labelId;
        this.iconId = iconId;
        this.types = types;
    }

    public BillPaymentType[] getTypes() {
        return types;
    }

    public boolean hasTypes() {
        return types.length > 1;
    }

    @Override
    public int getIconResourceId() {
        return iconId;
    }

    @Override
    public int getLabelResourceId() {
        return labelId;
    }

    @Override
    public int getId() {
        return ordinal();
    }
}
