package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 3/15/18.
 */

public enum BillPaymentType implements SpinnerItem {
    PSTN("Telkom", new Biller[] { Biller.S_TELKOM }),
    POSTPAID_ELECTRIC("PLN", new Biller[] { Biller.S_PLN_POSTPAID }),
    PREPAID_ELECTRIC("Token PLN", new Biller[] { Biller.S_PLN_PREPAID }),
    TAP_WATER("PAM", new Biller[] { Biller.TW_AETRA, Biller.TW_PALYJA }),
    NATIONAL_INSURANCE("BPJS", new Biller[] { Biller.S_BPJS_SEHAT }),
    PERSONAL_INSURANCE("Asuransi", new Biller[] { Biller.PI_SINARMAS, Biller.PI_SINARMAS_LIFE, Biller.PI_AIG, Biller.PI_PRUDENTIAL }),
    TV_CABLE("TV Kabel", new Biller[] { Biller.CTV_INDOVISION, Biller.CTV_FIRST_MEDIA, Biller.CTV_YES_TV, Biller.CTV_TELKOM_VISION, Biller.CTV_INNOVATE, Biller.CTV_BIG }),
    INTERNET("Internet", new Biller[] { Biller.ISP_INDIHOME, Biller.ISP_INDOSAT_NET, Biller.ISP_CENTRIN_NET }),
    TRAVEL_TICKET("Tiket", new Biller[] { Biller.TRV_LION_AIR, Biller.TRV_GARUDA_AIR, Biller.TRV_BATIK_AIR, Biller.TRV_WING_AIR, Biller.TRV_SRIWIJAYA_AIR, Biller.TRV_KERETA_API }),
    CREDIT_CARD("Kartu Kredit", new Biller[] { Biller.CC_CITIBANK, Biller.CC_BNI, Biller.CC_ANZ, Biller.CC_PERMATA, Biller.CC_DANAMON, Biller.CC_UOB, Biller.CC_PANIN, Biller.CC_BUKOPIN, Biller.CC_ICB_BUMIPUTERA }),
    LOAN("Pinjaman", new Biller[] { Biller.PL_ANZ, Biller.PL_CITIBANK_EP, Biller.PL_CITIBANK_PL_RC }),
    PREPAID_CELLPHONE("Voucher Prabayar", new Biller[] { Biller.PRE_MENTARI, Biller.PRE_IM3, Biller.PRE_SIMPATI, Biller.PRE_AS, Biller.PRE_XL, Biller.PRE_3, Biller.PRE_AXIS, Biller.PRE_SMART }),
    POSTPAID_CELLPHONE("HP Pascabayar", new Biller[] { Biller.POST_TELKOMSEL, Biller.POST_INDOSAT, Biller.POST_XPLOR, Biller.POST_PSN_BYRU, Biller.POST_SMARTFREN });

    private final String name;
    private final Biller[] billers;

    BillPaymentType(String name, Biller[] billers) {
        this.name = name;
        this.billers = billers;
    }

    public String getName() {
        return name;
    }

    public Biller[] getBillers() {
        return billers;
    }

    public boolean hasBillers() {
        return billers.length > 1;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getLabel() {
        return name;
    }

//    public boolean isOpenAmount() {
//        return this == PERSONAL_INSURANCE || this == TV_CABLE || this == CREDIT_CARD || this == LOAN || this == INTERNET;
//    }

//    public boolean isNonInquiry() {
//        return this == PREPAID_CELLPHONE || this == CREDIT_CARD || this == PERSONAL_INSURANCE || this == LOAN || this == INTERNET;
//    }

//    public boolean canSave() {
//        return this != PREPAID_ELECTRIC && this != PREPAID_CELLPHONE;
//    }
}
