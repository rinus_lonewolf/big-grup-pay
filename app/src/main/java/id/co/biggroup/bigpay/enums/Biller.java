package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 3/19/18.
 */

public enum Biller implements SpinnerItem {
    S_TELKOM("telkomPstn", "Telkom"),
    S_BPJS_SEHAT("bpjsKesehatan", "BPJS Kesehatan"),
    S_PLN_POSTPAID("plnPostpaid", "PLN"),
    S_PLN_PREPAID("plnPrepaid", "Token PLN"),
    TW_PALYJA("pamPalyja", "PAM Palyja"),
    TW_AETRA("pamAetra", "PAM Aetra"),
    TW_PDAM("pdam", "PDAM"),
    CC_CITIBANK("citibank", "Citibank"),
    CC_BNI("bni", "BNI"),
    CC_ANZ("anz", "ANZ / RBS"),
    CC_PERMATA("permata", "Permata Bank / GE Finance"),
    CC_DANAMON("danamon", "Bank Danamon"),
    CC_UOB("uob", "UOB"),
    CC_PANIN("panin", "Panin"),
    CC_BUKOPIN("bukopin", "Bukopin"),
    CC_ICB_BUMIPUTERA("icbBumiPutra", "ICB Bumiputera / MNC Bank"),
    ISP_CBN("cbn", "CBN"),
    ISP_INDIHOME("telkomPstn", "IndiHome / Telkom Speedy"),
    ISP_INDOSAT_NET("indosatnet", "Indosatnet"),
    ISP_CENTRIN_NET("centrinnet", "Centrinnet"),
    CTV_INDOVISION("indovision", "Indovision / MNC Vision"),
    CTV_FIRST_MEDIA("firstMedia", "First Media"),
    CTV_YES_TV("yesTv", "Yes TV"),
    CTV_OKE_TV("okeTv", "Oke TV"),
    CTV_TOP_TV("topTv", "Top TV"),
    CTV_AORA_TV("aoraTv", "Aora TV"),
    CTV_TELKOM_VISION("telkomVision", "Trans Vision / Telkom Vision"),
    CTV_INNOVATE("innovate", "Innovate / MyRepublic"),
    CTV_BIG("big", "BIG TV"),
    PL_ANZ("anzPl", "ANZ PL"),
    PL_CITIBANK_EP("citibankEasyPay", "Citibank PL"),
    PL_CITIBANK_PL_RC("citibankPlReadyCash", "Citibank Ready Cash"),
    PL_GE_FINANCE("geFinanceInstallment", "GE Finance"),
    PL_WOM_FINANCE("womFinance", "WOM Finance"),
    PL_KTA_PERMATA("ktaPermata", "KTA Permata"),
    PI_SINARMAS_LIFE("sinarmasLife", "Sinarmas Life"),
    PI_SINARMAS("sinarmas", "Asuransi Sinar Mas"),
    PI_AIG("aig", "AIA Financial"),
    PI_PRUDENTIAL("prudential", "Asuransi Prudential"),
    TRV_LION_AIR("lionAir", "Lion Air"),
    TRV_GARUDA_AIR("garudaAir", "Garuda Indonesia"),
    TRV_BATIK_AIR("batikAir", "Batik Air"),
    TRV_WING_AIR("wingAir", "Wing Air"),
    TRV_SRIWIJAYA_AIR("sriwijayaAir", "Sriwijaya Air"),
    TRV_KERETA_API("keretaApi", "Kereta Api"),
    POST_TELKOMSEL("telkomselHalo", "Telkomsel kartuHalo"),
    POST_INDOSAT("indosatMatrix", "Indosat Matrix Ooredoo"),
    POST_XPLOR("xplor", "XL Prioritas"),
    POST_PSN_BYRU("psnByru", "PSN / BYRU"),
    POST_SMARTFREN("smartfren", "Smartfren Pascabayar"),
    POST_AXIS("axis", "Axis"),
    PRE_MENTARI("indosatMentariPrepaid", "Mentari"),
    PRE_IM3("indosatIm3Prepaid", "Indosat IM3"),
    PRE_SIMPATI("telkomselSimpatiPrepaid", "Telkomsel Simpati"),
    PRE_AS("telkomselAsPrepaid", "Telkomsel AS"),
    PRE_XL("xlBebasPrepaid", "XL"),
    PRE_3("3Prepaid", "Tri"),
    PRE_AXIS("axisPrepaid", "Axis"),
    PRE_SMART("smartPrepaid", "Smartfren"),
    ;

    private final String code;
    private final String name;

    Biller(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getLabel() {
        return name;
    }

    public SubBiller[] getSubBillers() {
        SubBiller[] subBillers = new SubBiller[0];
        if (TW_PDAM.equals(this)) {
            subBillers = new SubBiller[] {
                SubBiller.PDAM_BALIKPAPAN, SubBiller.PDAM_BOYOLALI,
                SubBiller.PDAM_CILACAP, SubBiller.PDAM_DENPASAR,
                SubBiller.PDAM_MAKASAR, SubBiller.PDAM_MANADO,
                SubBiller.PDAM_PONTIANAK, SubBiller.PDAM_SEMARANG,
                SubBiller.PDAM_KUBURAYA, SubBiller.PDAM_BANDUNG,
                SubBiller.PDAM_MALANG, SubBiller.PDAM_JAMBI,
                SubBiller.PDAM_LAMPUNG, SubBiller.PDAM_PALEMBANG,
                SubBiller.PDAM_BOGOR, SubBiller.PDAM_BERAU,
                SubBiller.PDAM_GROGOT, SubBiller.PDAM_SUKABUMI,
                SubBiller.PDAM_BONDOWOSO, SubBiller.PDAM_BEKASI,
                SubBiller.PDAM_BANTUL, SubBiller.PDAM_SLEMAN,
            };
        }
        if (CTV_BIG.equals(this)) {
            subBillers = new SubBiller[] {
                SubBiller.BIG_CLOSED, SubBiller.BIG_OPEN, //SubBiller.BIG_PREPAID
            };
        }
        return subBillers;
    }

    public boolean hasSubBillers() {
        return getSubBillers().length > 1;
    }

    public Integer[] getDenoms() {
        switch (this) {
            case S_PLN_PREPAID:
                return new Integer[] { 20000, 50000, 100000, 200000, 500000, 1000000 };
            case PRE_MENTARI:
            case PRE_SMART:
                return new Integer[] { 25000, 50000, 100000 };
            case PRE_IM3:
                return new Integer[] { 25000, 50000, 100000, 150000 };
            case PRE_SIMPATI:
            case PRE_AS:
                return new Integer[] { 10000, 20000, 25000, 50000, 100000, 150000 };
            case PRE_XL:
            case PRE_3:
                return new Integer[] { 10000, 25000, 30000, 50000, 100000 };
            case PRE_AXIS:
                return new Integer[] { 10000, 25000, 50000, 100000 };
        }
        return new Integer[0];
    }

    public boolean hasDenoms() {
        return getDenoms().length > 1;
    }

    public boolean isNonInquiry() {
        return
          (this.hasDenoms() && this != S_PLN_PREPAID) ||
          this == POST_PSN_BYRU ||
          this == CC_CITIBANK || this == CC_BNI || this == CC_ANZ || this == CC_PERMATA || this == CC_DANAMON ||
          this == CC_UOB || this == CC_PANIN || this == CC_BUKOPIN || this == CC_ICB_BUMIPUTERA ||
          this == PL_ANZ || this == PL_CITIBANK_EP || this == PL_CITIBANK_PL_RC ||
          this == CTV_INDOVISION || this == CTV_FIRST_MEDIA || this == CTV_YES_TV ||
          this == PI_SINARMAS || this == PI_SINARMAS_LIFE || this == PI_AIG || this == PI_PRUDENTIAL ||
          this == ISP_INDOSAT_NET || this == ISP_CENTRIN_NET;
    }

    public boolean isOpenAmount() {
        return
            this == POST_PSN_BYRU ||
            this == CC_CITIBANK || this == CC_BNI || this == CC_ANZ || this == CC_PERMATA || this == CC_DANAMON ||
            this == CC_UOB || this == CC_PANIN || this == CC_BUKOPIN || this == CC_ICB_BUMIPUTERA ||
            this == PL_ANZ || this == PL_CITIBANK_EP || this == PL_CITIBANK_PL_RC ||
            this == CTV_INDOVISION || this == CTV_FIRST_MEDIA || this == CTV_YES_TV ||
            this == PI_SINARMAS || this == PI_SINARMAS_LIFE || this == PI_AIG || this == PI_PRUDENTIAL ||
            this == ISP_INDOSAT_NET || this == ISP_CENTRIN_NET;
    }
}
