package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.model.GridMenuItem;

public enum BuildingInfoItem implements GridMenuItem
{
  MAJESTY_HOTEL(R.drawable.ic_majesty, R.string.hotel_the_majesty, "https://themajestyhotel.co.id/"),
  GRAND_SETIABUDI_HOTEL(R.drawable.ic_setiabudi, R.string.hotel_grand_setiabudi, "https://grandsetiabudihotel.co.id/"),
  GALERI_CIUMBULEUIT_HOTEL(R.drawable.ic_ciumbuleuit, R.string.hotel_galeri_ciumbuleuit, "https://galericiumbuleuithotel.co.id/"),
  BTC_HOTEL(R.drawable.ic_btc_hotel, R.string.hotel_btc, "https://btc-hotel.co.id/"),
  GREEN_FOREST(R.drawable.ic_greenforest, R.string.hotel_green_forest, "https://thegreenforestresort.co.id/"),
  HEGARMANAH(R.drawable.ic_hegarmanah, R.string.residential_hegarmanah, "https://hegarmanahresidence.com/"),
  GALERI_CIUMBULEUIT_2(R.drawable.ic_ciumbuleuit, R.string.residential_galeri_ciumbuleuit_2, "http://www.galericiumbuleuit2.com/"),
  GALERI_CIUMBULEUIT_3(R.drawable.ic_ciumbuleuit, R.string.residential_galeri_ciumbuleuit_3, "http://www.galericiumbuleuit3.com/"),
  PINEWOOD(R.drawable.ic_pinewood, R.string.residential_pinewood, "http://pinewood-apartment.co.id/"),
  BTC_FASHION_MALL(R.drawable.ic_btc_mall, R.string.mall_btc_fashion, "http://btcfashionmall.com/"),
  SOLO_GRAND_MALL(R.drawable.ic_solo_grand_mall, R.string.mall_solo_grand, "http://www.solograndmall.com/"),
  SOLO_PARAGON(R.drawable.ic_solo_paragon, R.string.mall_solo_paragon, "http://solo-paragon.com/"),
  JATINANGOR_TOWN_SQUARE(R.drawable.ic_jatinangor, R.string.mall_jatinangor_town_square, "http://www.jatosmall.com/");

  private final int iconId;
  private final int labelId;
  private final String url;

  BuildingInfoItem(int iconId, int labelId, String url) {
    this.iconId = iconId;
    this.labelId = labelId;
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  @Override
  public int getIconResourceId()
  {
    return iconId;
  }

  @Override
  public int getLabelResourceId()
  {
    return labelId;
  }

  @Override
  public int getId()
  {
    return ordinal();
  }
}
