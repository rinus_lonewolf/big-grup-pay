package id.co.biggroup.bigpay.enums;

/**
 * Created by developer on 5/8/18.
 */
public enum Feature {
    SIGN_IN("00", Module.USER_ACCOUNT),
    SIGN_OUT("01", Module.USER_ACCOUNT),
    SIGN_UP("02", Module.USER_ACCOUNT),
    VERIFY("03", Module.USER_ACCOUNT),
    RESET_KEY("04", Module.USER_ACCOUNT),
    CHANGE_KEY("05", Module.USER_ACCOUNT),
    PROFILE("06", Module.USER_ACCOUNT),
    TT_INQUIRY("00", Module.TUITION),
    ;

    private final String code;
    private final Module module;

    Feature(String code, Module module) {
        this.code = code;
        this.module = module;
    }

    public String getCode() {
        return code;
    }

    public Module getModule() {
        return module;
    }
}
