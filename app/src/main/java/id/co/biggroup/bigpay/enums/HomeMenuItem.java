package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.act.BPInitAct;
import id.co.biggroup.bigpay.act.BuildingInfoAct;
import id.co.biggroup.bigpay.act.VAAct;
import id.co.biggroup.bigpay.model.GridMenuItem;

/**
 * Created by developer on 3/22/18.
 */

public enum HomeMenuItem implements GridMenuItem {
    RESIDENTIAL_BILL(R.drawable.ic_residential_pay, R.string.title_va, VAAct.class, null),
    QR_PAYMENT(R.drawable.ic_qrpay, R.string.title_qrpay, null, null),
    BILL_PAYMENT(R.drawable.ic_bill_pay, R.string.title_billpay, BPInitAct.class, null),
//    BILL_PAYMENT(R.drawable.ic_billpay, R.string.title_billpay, null, null),
//    QR_PAYMENT(R.drawable.ic_qrpay, R.string.title_qrpay, ScanAct.class, null),
    BUILDING_INFO(R.drawable.ic_building, R.string.label_hotel_resident_mall, BuildingInfoAct.class, null),
    CHAT_ON_WA(R.drawable.ic_whatsapp, R.string.label_chat_whatsapp, null, "https://wa.me/628112289092");

    private final int iconId;
    private final int labelId;
    private final Class activity;
    private final String url;

    HomeMenuItem(int iconId, int labelId, Class activity, String url) {
        this.iconId = iconId;
        this.labelId = labelId;
        this.activity = activity;
        this.url = url;
    }

    public Class getActivity() {
        return activity;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public int getIconResourceId() {
        return iconId;
    }

    @Override
    public int getLabelResourceId() {
        return labelId;
    }

    @Override
    public int getId() {
        return ordinal();
    }
}
