package id.co.biggroup.bigpay.enums;

/**
 * Created by developer on 5/8/18.
 */

public enum Module {
    USER_ACCOUNT("UA"),
    TUITION("TT"),
    ;

    private final String code;

    Module(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
