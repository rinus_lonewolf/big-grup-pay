package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 5/3/18.
 */

public enum PayMethod implements SpinnerItem {
    BCA("Virtual Account BCA", "BCA"),
    MANDIRI("Virtual Account Mandiri", "Mandiri"),
    WALLET("B.Pay Wallet", "Wallet"),
    B_PAY("B.Pay", null),
    ;

    private final String label;
    private final String key;

    PayMethod(String label, String key) {
        this.label = label;
        this.key = key;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getLabel() {
        return label;
    }

    public String getKey() {
        return key;
    }
}
