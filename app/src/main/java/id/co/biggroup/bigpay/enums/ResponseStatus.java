package id.co.biggroup.bigpay.enums;

import org.apache.commons.lang3.StringUtils;

import id.co.biggroup.bigpay.R;

public enum ResponseStatus {
    SIGN_IN_INVALID(Feature.SIGN_IN, Status.CODE_10, R.string.status_signin_invalid),
    SIGN_IN_BLOCKED(Feature.SIGN_IN, Status.CODE_11, R.string.status_signin_blocked),
    SIGN_IN_DISABLED(Feature.SIGN_IN, Status.CODE_12, R.string.status_signin_disabled),
    SIGN_IN_USED(Feature.SIGN_IN, Status.CODE_13, R.string.status_signin_used),
    SIGN_IN_UNVERIFIED(Feature.SIGN_IN, Status.CODE_20, R.string.status_signin_unverified),
    SIGN_IN_ALREADY_ON_OTHERS(Feature.SIGN_IN, Status.CODE_21, R.string.status_signin_different_channel),
    SIGN_IN_UNREGISTERED(Feature.SIGN_IN, Status.CODE_22, R.string.status_signin_unregistered),

    SIGN_UP_EMPTY_NAME(Feature.SIGN_UP, Status.CODE_10, R.string.status_signup_empty_name),
    SIGN_UP_EMPTY_EMAIL(Feature.SIGN_UP, Status.CODE_11, R.string.status_signup_empty_email),
    SIGN_UP_EMPTY_HP(Feature.SIGN_UP, Status.CODE_12, R.string.status_signup_empty_hp),
    SIGN_UP_INVALID_EMAIL(Feature.SIGN_UP, Status.CODE_13, R.string.status_signup_invalid_email),
    SIGN_UP_INVALID_HP(Feature.SIGN_UP, Status.CODE_14, R.string.status_signup_invalid_hp),
    SIGN_UP_USED_EMAIL(Feature.SIGN_UP, Status.CODE_15, R.string.status_signup_used_email),
    SIGN_UP_USED_HP(Feature.SIGN_UP, Status.CODE_16, R.string.status_signup_used_hp),
    SIGN_UP_EMPTY_KEY(Feature.SIGN_UP, Status.CODE_17, R.string.status_signup_empty_key),

    VERIFY_EMPTY_CODE(Feature.VERIFY, Status.CODE_10, R.string.status_verify_empty_code),
    VERIFY_INVALID_CODE(Feature.VERIFY, Status.CODE_11, R.string.status_verify_invalid_code),
    VERIFY_UNSENT_EMAIL(Feature.VERIFY, Status.CODE_12, R.string.status_verify_unsent_email),
    VERIFY_VERIFIED(Feature.VERIFY, Status.CODE_14, R.string.status_verify_verified),

    RESET_KEY_EMPTY_EMAIL(Feature.RESET_KEY, Status.CODE_10, R.string.status_reset_key_empty_email),
    RESET_KEY_INVALID_EMAIL(Feature.RESET_KEY, Status.CODE_11, R.string.status_reset_key_invalid_email),

    CHANGE_KEY_EMPTY_OLD_KEY(Feature.CHANGE_KEY, Status.CODE_10, R.string.status_change_key_empty_old_key),
    CHANGE_KEY_EMPTY_NEW_KEY(Feature.CHANGE_KEY, Status.CODE_11, R.string.status_change_key_empty_new_key),
    CHANGE_KEY_INVALID_OLD_KEY(Feature.CHANGE_KEY, Status.CODE_12, R.string.status_change_key_invalid_old_key),
    CHANGE_KEY_SIMILAR_KEYS(Feature.CHANGE_KEY, Status.CODE_13, R.string.status_change_key_similar_keys),
    CHANGE_KEY_INVALID_NEW_KEY(Feature.CHANGE_KEY, Status.CODE_14, R.string.status_change_key_invalid_new_key),

    TT_INQUIRY_INVALID_REG_NUM(Feature.TT_INQUIRY, Status.CODE_10, R.string.status_tt_inquiry_invalid_reg_num),
    TT_INQUIRY_DUPLICATE_REG_NUM(Feature.TT_INQUIRY, Status.CODE_11, R.string.status_tt_inquiry_duplicate_reg_num),
    TT_INQUIRY_NOT_EXISTS_REG_NUM(Feature.TT_INQUIRY, Status.CODE_12, R.string.status_tt_inquiry_not_exists_reg_num),
    TT_INQUIRY_INVALID_TYPE(Feature.TT_INQUIRY, Status.CODE_13, R.string.status_tt_inquiry_invalid_type),
    TT_INQUIRY_ACCESS_DENIED(Feature.TT_INQUIRY, Status.CODE_14, R.string.status_tt_inquiry_access_denied),

    COMMON_SUCCESS(null, Status.SUCCESS, R.string.status_success),
    COMMON_TOKEN(null, Status.INVALID_TOKEN, R.string.status_invalid_token),
    COMMON_CHECKSUM(null, Status.INVALID_CHECKSUM, R.string.status_invalid_checksum),
    COMMON_SYSTEM(null, Status.SYSTEM_ERROR, R.string.status_system_error),

    BILLPAY_SUCCES(null, Status.SUCCESS_BILLPAY, R.string.status_success),
    BILLPAY_MESSAGE_ERROR(null, Status.CODE_11, R.string.status_billpay_message_error),
    BILLPAY_UNKNOWN_SERVICE_CODE(null, Status.CODE_12, R.string.status_billpay_unknown_service_code),
    BILLPAY_AMOUNT_DIFFERENCE(null, Status.CODE_13, R.string.status_billpay_amount_difference),
    BILLPAY_UNKNOWN_BILL(null, Status.CODE_14, R.string.status_billpay_unknown_bill),
    BILLPAY_TRX_ERROR(null, Status.CODE_20, R.string.status_billpay_trx_error),
    BILLPAY_UNKNOWN_WRONG_USER_PASSWORD(null, Status.CODE_27, R.string.status_billpay_unknown_wrong_user_pass),
    BILLPAY_UNSUPPORTED_TRX(null, Status.CODE_40, R.string.status_billpay_unsupported_trx),
    BILLPAY_TIMEOUT(null, Status.CODE_68, R.string.status_billpay_timeout),
    BILLPAY_ALREADY_PAID(null, Status.CODE_88, R.string.status_billpay_already_paid),

    UNDEFINED(null, null, R.string.status_billpay_error),
    ;

    private final Feature feature;
    private final Status status;
    private final int messageId;

    ResponseStatus(Feature feature, Status status, int messageId) {
        this.feature = feature;
        this.status = status;
        this.messageId = messageId;
    }

    public Feature getFeature() {
        return feature;
    }

    public Status getStatus() {
        return status;
    }

    public int getMessageId() {
        return messageId;
    }

    public static ResponseStatus getByValue(String value) {
        String module = "", feature = "", status = "";

        if (StringUtils.isBlank(value)) {
            return UNDEFINED;
        }
        else if (value.length() == 2 || value.equals("0")) {
            status = value;
        }
        else if (value.length() == 8) {
            module = value.substring(2, 4);
            feature = value.substring(4, 6);
            status = value.substring(6, 8);
        }

        for (ResponseStatus entry : ResponseStatus.values()) {
            if (entry.getFeature() == null) {
              if (entry.getStatus() == null)
                continue;
              if (!entry.getStatus().getCode().equals(status))
                continue;
            }
            if (entry.getFeature() != null) {
              if (!entry.getFeature().getModule().getCode().equals(module))
                continue;
              if (!entry.getFeature().getCode().equals(feature))
                continue;
              if (!entry.getStatus().getCode().equals(status))
                continue;
            }
            return entry;
        }
        return UNDEFINED;
    }
}
