package id.co.biggroup.bigpay.enums;

/**
 * Created by developer on 5/8/18.
 */
public enum Status {
    SUCCESS("00"), INVALID_TOKEN("01"), INVALID_CHECKSUM("05"), SYSTEM_ERROR("09"), CODE_10("10"),
    CODE_11("11"), CODE_12("12"), CODE_13("13"), CODE_14("14"), CODE_15("15"), CODE_16("16"),
    CODE_17("17"), CODE_20("20"), CODE_21("21"), CODE_22("22"), CODE_27("27"), CODE_40("40"),
    CODE_68("68"), CODE_88("88"), SUCCESS_BILLPAY("0");

    private final String code;

    Status(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
