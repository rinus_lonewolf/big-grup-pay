package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 3/19/18.
 */

public enum SubBiller implements SpinnerItem {
    PDAM_BALIKPAPAN("pdamBalikPapan", "PDAM Balikpapan"),
    PDAM_BOYOLALI("pdamBoyolali", "PDAM Boyolali"),
    PDAM_CILACAP("pdamCilacap", "PDAM Cilacap"),
    PDAM_DENPASAR("pdamDenpasar", "PDAM Denpasar"),
    PDAM_MAKASAR("pdamMakasar", "PDAM Makasar"),
    PDAM_MANADO("pdamManado", "PDAM Manado"),
    PDAM_PONTIANAK("pdamPontianak", "PDAM Pontianak"),
    PDAM_SEMARANG("pdamSemarang", "PDAM Semarang"),
    PDAM_KUBURAYA("pdamKuburaya", "PDAM Kuburaya"),
    PDAM_BANDUNG("pdamBandung", "PDAM Bandung"),
    PDAM_MALANG("pdamMalang", "PDAM Malang"),
    PDAM_JAMBI("pdamJambi", "PDAM Jambi"),
    PDAM_LAMPUNG("pdamLampung", "PDAM Lampung"),
    PDAM_PALEMBANG("pdamPalembang", "PDAM Palembang"),
    PDAM_BOGOR("pdamBogor", "PDAM Bogor"),
    PDAM_BERAU("pdamBerau", "PDAM Berau"),
    PDAM_GROGOT("pdamGrogot", "PDAM Grogot"),
    PDAM_SUKABUMI("pdamSukabumi", "PDAM Sukabumi"),
    PDAM_BONDOWOSO("pdamBondowoso", "PDAM Bondowoso"),
    PDAM_BEKASI("pdamBekasi", "PDAM Bekasi"),
    PDAM_BANTUL("pdamBantul", "PDAM Bantul"),
    PDAM_SLEMAN("pdamSleman", "PDAM Sleman"),
    BIG_CLOSED("bigClosed", "Closed Payment"),
    BIG_OPEN("bigOpen", "Open Payment"),
    BIG_PREPAID("bigPrepaid", "Prepaid"),
    ;

    private final String code;
    private final String name;

    SubBiller(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getLabel() {
        return name;
    }

    public boolean isOpenAmount() {
        return this == BIG_OPEN;
    }
}
