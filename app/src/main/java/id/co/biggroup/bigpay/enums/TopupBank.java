package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 4/6/18.
 */

public enum TopupBank {
    BCA(R.string.topup_bank_bca, new TopupChannel[] { TopupChannel.BCA_ATM, TopupChannel.BCA_MOBILE, TopupChannel.BCA_SIM }, "79001"),
    PERMATA(R.string.topup_bank_permata, new TopupChannel[] { TopupChannel.PMT_ATM, TopupChannel.PMT_MOBILE, TopupChannel.PMT_NET }, "85070"),
    BII(R.string.topup_bank_bii, new TopupChannel[] { TopupChannel.ATM_BERSAMA, TopupChannel.ATM_PRIMA }, "78388"),
    OTHERS(R.string.topup_bank_others, new TopupChannel[] { TopupChannel.ATM_BERSAMA, TopupChannel.ATM_PRIMA }, "78388");

    private final int label;
    private final TopupChannel[] channels;
    private final String bin;

    TopupBank(int label, TopupChannel[] channels, String bin) {
        this.label = label;
        this.channels = channels;
        this.bin = bin;
    }

    public int getLabel() {
        return label;
    }

    public TopupChannel[] getChannels() {
        return channels;
    }

    public String getBin() {
        return bin;
    }
}
