package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 4/6/18.
 */

public enum TopupChannel {
    BCA_ATM(R.string.topup_channel_bca_atm, R.array.topup_step_bca_atm),
    BCA_MOBILE(R.string.topup_channel_bca_mobile, R.array.topup_step_bca_mobile),
    BCA_SIM(R.string.topup_channel_bca_sim, R.array.topup_step_bca_sim),
    PMT_ATM(R.string.topup_channel_pmt_atm, R.array.topup_step_pmt_atm),
    PMT_MOBILE(R.string.topup_channel_pmt_mobile, R.array.topup_step_pmt_mobile),
    PMT_NET(R.string.topup_channel_pmt_net, R.array.topup_step_pmt_net),
    ATM_BERSAMA(R.string.topup_channel_atm_bersama, R.array.topup_step_atm_bersama),
    ATM_PRIMA(R.string.topup_channel_atm_prima, R.array.topup_step_atm_prima);

    private final int label;
    private final int steps;

    TopupChannel(int label, int steps) {
        this.label = label;
        this.steps = steps;
    }

    public int getLabel() {
        return label;
    }

    public int getSteps() {
        return steps;
    }
}
