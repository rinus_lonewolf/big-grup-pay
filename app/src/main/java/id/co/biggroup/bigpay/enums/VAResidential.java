package id.co.biggroup.bigpay.enums;

import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 5/3/18.
 */

public enum VAResidential implements SpinnerItem {
    MAJESTY("TM01", "The Majesty");

    private final String key;
    private final String label;

    VAResidential(String key, String label) {
        this.key = key;
        this.label = label;
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getLabel() {
        return label;
    }

    public String getKey() {
        return key;
    }
}
