package id.co.biggroup.bigpay.form;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.io.Serializable;

import id.co.biggroup.bigpay.BR;
import id.co.biggroup.bigpay.enums.BillPaymentGroup;
import id.co.biggroup.bigpay.enums.BillPaymentType;
import id.co.biggroup.bigpay.enums.Biller;
import id.co.biggroup.bigpay.enums.SubBiller;
import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 3/15/18.
 */

public class BPForm extends BaseObservable implements Serializable {
    private BillPaymentGroup group;
    private BillPaymentType type;
    private Biller biller;
    private SubBiller subBiller;
    private String billId;
    private String auxBillId;
    private SpinnerItem month;
    private Long amount;
    private SpinnerItem denom;
//    private Boolean saveTrx = false;
//    private String alias;
    public String items;
    public Long total;
    public Long denominasi;

    @Bindable
    public BillPaymentGroup getGroup() {
        return group;
    }

    public void setGroup(BillPaymentGroup group) {
        this.group = group;
        notifyPropertyChanged(BR.group);
    }

    @Bindable
    public BillPaymentType getType() {
        return type;
    }

    public void setType(BillPaymentType type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }

    @Bindable
    public Biller getBiller() {
        return biller;
    }

    public void setBiller(Biller biller) {
        this.biller = biller;
        notifyPropertyChanged(BR.biller);
    }

    @Bindable
    public SubBiller getSubBiller() {
        return subBiller;
    }

    public void setSubBiller(SubBiller subBiller) {
        this.subBiller = subBiller;
        notifyPropertyChanged(BR.subBiller);
    }

    @Bindable
    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
        notifyPropertyChanged(BR.billId);
    }

    @Bindable
    public String getAuxBillId() {
        return auxBillId;
    }

    public void setAuxBillId(String auxBillId) {
        this.auxBillId = auxBillId;
        notifyPropertyChanged(BR.auxBillId);
    }

    @Bindable
    public SpinnerItem getMonth() {
        return month;
    }

    public void setMonth(SpinnerItem month) {
        this.month = month;
        notifyPropertyChanged(BR.month);
    }

    @Bindable
    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
        notifyPropertyChanged(BR.amount);
    }

    @Bindable
    public SpinnerItem getDenom() {
        return denom;
    }

    public void setDenom(SpinnerItem denom) {
        this.denom = denom;
        notifyPropertyChanged(BR.denom);
    }

//    @Bindable
//    public Boolean getSaveTrx() {
//        return saveTrx;
//    }
//
//    public void setSaveTrx(Boolean saveTrx) {
//        this.saveTrx = saveTrx;
//        notifyPropertyChanged(BR.saveTrx);
//    }
//
//    @Bindable
//    public String getAlias() {
//        return alias;
//    }
//
//    public void setAlias(String alias) {
//        this.alias = alias;
//        notifyPropertyChanged(BR.alias);
//    }
}
