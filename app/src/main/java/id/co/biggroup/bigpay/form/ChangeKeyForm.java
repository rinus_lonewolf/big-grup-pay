package id.co.biggroup.bigpay.form;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import id.co.biggroup.bigpay.BR;

/**
 * Created by developer on 3/27/18.
 */

public class ChangeKeyForm extends BaseObservable {
    private String oldKey;
    private String newKey;
    private String confirmNewKey;

    @Bindable
    public String getOldKey() {
        return oldKey;
    }

    public void setOldKey(String oldKey) {
        this.oldKey = oldKey;
        notifyPropertyChanged(BR.oldKey);
    }

    @Bindable
    public String getNewKey() {
        return newKey;
    }

    public void setNewKey(String newKey) {
        this.newKey = newKey;
        notifyPropertyChanged(BR.newKey);
    }

    @Bindable
    public String getConfirmNewKey() {
        return confirmNewKey;
    }

    public void setConfirmNewKey(String confirmNewKey) {
        this.confirmNewKey = confirmNewKey;
        notifyPropertyChanged(BR.confirmNewKey);
    }
}
