package id.co.biggroup.bigpay.form;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.io.Serializable;
import java.util.Date;

import id.co.biggroup.bigpay.BR;
import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by developer on 2/22/18.
 */

public class QRInputForm extends BaseObservable implements Serializable {
    public final static int DISCOUNT_RATE = 10;
    private SpinnerItem payMethod;

    public int step = Constant.STEP_SCAN;
    public String merchantName;
    public Date date = new Date();
    public String currency;
    public Long amount;
    public String ecommRefNo;
    public String paymentMethod;
    public boolean paymentSuccess = false;

    @Bindable
    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
        notifyPropertyChanged(BR.step);
    }

    @Bindable
    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
        notifyPropertyChanged(BR.merchantName);
    }

    @Bindable
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        notifyPropertyChanged(BR.date);
    }

    @Bindable
    public SpinnerItem getPayMethod() {
      return payMethod;
    }

    public void setPayMethod(SpinnerItem payMethod) {
      this.payMethod = payMethod;
      notifyPropertyChanged(BR.payMethod);
    }

    @Bindable
    public String getCurrency() {
      return currency;
    }

    public void setCurrency(String currency) {
      this.currency = currency;
      notifyPropertyChanged(BR.currency);
    }

    @Bindable
    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
        notifyPropertyChanged(BR.amount);
    }

    @Bindable
    public Long getDiscount() {
        if (amount == null || amount == 0) {
            return 0L;
        }
        return DISCOUNT_RATE * amount / 100;
    }

    @Bindable
    public Long getTotal() {
        if (amount == null || amount == 0) {
            return 0L;
        }
        return amount - getDiscount();
    }
}
