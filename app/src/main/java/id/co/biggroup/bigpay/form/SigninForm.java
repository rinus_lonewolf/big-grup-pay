package id.co.biggroup.bigpay.form;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.io.Serializable;

import id.co.biggroup.bigpay.BR;

/**
 * Created by developer on 2/21/18.
 */

public class SigninForm extends BaseObservable implements Serializable {
    private String email;
    private String key;

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
        notifyPropertyChanged(BR.key);
    }
}
