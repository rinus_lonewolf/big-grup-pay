package id.co.biggroup.bigpay.form;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.io.Serializable;
import java.math.BigDecimal;

import id.co.biggroup.bigpay.BR;
import id.co.biggroup.bigpay.api.response.VA;
import id.co.biggroup.bigpay.enums.PayMethod;
import id.co.biggroup.bigpay.enums.VAResidential;
import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by dolan on 2/22/18.
 */

public class VAForm extends BaseObservable implements Serializable {
    private String regNum;
    private SpinnerItem residential;
    private SpinnerItem payMethod;

    public String name;
    public String period;
    public Long monthlyCharge;
    public Long ppn;
    public Long watery;
    public Long electricity;
    public Long electricSubscription;
    public BigDecimal publicLightning;
    public Long changingFixing;
    public Long administrationFee;
    public Long waterMaintenance;
    public Long stamp;
    public Long penalty;
    public Long fee;
    public BigDecimal payAmount;
    public Long amount;
    public VA[] vas;
    public String dueDate;
    public Integer paymentStatus;
    public String paidDate;

    public String daya_terpasang;
    public String kwh_listrik_awal;
    public String kwh_listrik_akhir;
    public String meter_awal_air;
    public String meter_akhir_air;
    public String unit;
    public String tipe;
    public String luas;
    public BigDecimal iuran_pemeliharaan;
    public BigDecimal ppn_new;
    public BigDecimal iuran_perbaikan_penggantian;
    public BigDecimal biaya_pemeliharaan_listrik;
    public BigDecimal total_biaya_listrik;
    public BigDecimal administrasi;
    public BigDecimal pju;
    public BigDecimal biaya_meter_air;
    public BigDecimal total_biaya_air;
    public BigDecimal jumlah_tagihan;
    public BigDecimal biaya_transaksi;
    @Bindable
    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
        notifyPropertyChanged(BR.regNum);
    }

    @Bindable
    public SpinnerItem getResidential() {
        return residential;
    }

    public void setResidential(SpinnerItem residential) {
        this.residential = residential;
        notifyPropertyChanged(BR.residential);
    }

    @Bindable
    public String getResidentialCode()
    {
        VAResidential vaResidential = (VAResidential) residential;
        if (VAResidential.MAJESTY.equals(vaResidential))
            return vaResidential.getKey();
        else
            return null;
    }

    @Bindable
    public SpinnerItem getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(SpinnerItem payMethod) {
        this.payMethod = payMethod;
        notifyPropertyChanged(BR.payMethod);
    }

    @Bindable
    public String getVaNumber() {
        PayMethod method = (PayMethod) payMethod;
        for (VA va : vas) {
            if (va.bank == null) {
                return null;
            }
            if (va.bank.contains(method.getKey())) {
                return va.no;
            }
        }
        return null;
    }

    @Bindable
    public String getPaymentStatusName() {
        if (paymentStatus == null) {
            return "Undefined";
        }
        if (paymentStatus == 1) {
            return "Paid";
        }
        if (paymentStatus == 2) {
            return "Expired";
        }
        return null;
    }
}
