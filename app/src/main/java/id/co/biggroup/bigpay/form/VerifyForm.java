package id.co.biggroup.bigpay.form;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import id.co.biggroup.bigpay.BR;

/**
 * Created by developer on 3/26/18.
 */

public class VerifyForm extends BaseObservable {
    private String code;

    @Bindable
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
        notifyPropertyChanged(BR.code);
    }
}
