package id.co.biggroup.bigpay.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.Calendar;
import java.util.Date;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.act.HistoryDetailAct;
import id.co.biggroup.bigpay.adapter.BPHistoryAdapter;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.ReportRequestBody;
import id.co.biggroup.bigpay.api.response.BPReportResponseBody;
import id.co.biggroup.bigpay.api.response.BPTrx;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.listener.EndlessScrollListener;
import id.co.biggroup.bigpay.util.PreferencesUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class BPHistoryFrag extends BaseFrag implements Response.Listener<BPReportResponseBody> {

    private SwipeRefreshLayout swipeRefreshLayout;
    private BPHistoryAdapter adapter;
    private TextView textInfo;
    private ListView listView;
    private boolean reload;

    private EndlessScrollListener scrollListener = new EndlessScrollListener(1) {
        @Override
        public boolean onLoadMore(int page, int totalItemsCount) {
            if (page > 1 && page > adapter.getTotalPage() || adapter.isLoading()) {
                return false;
            }
            loadData(page);
            return true;
        }
    };

    public BPHistoryFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_history_list, container, false);
        listView = view.findViewById(R.id.view_list);
        adapter = new BPHistoryAdapter(getContext());
        listView.setAdapter(adapter);
        listView.setOnScrollListener(scrollListener);
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            Intent i = new Intent(BPHistoryFrag.this.getContext(), HistoryDetailAct.class);
            i.putExtra(Constant.DATA_ITEM, (BPTrx) parent.getAdapter().getItem(position));
            startActivity(i);
        });
        swipeRefreshLayout = view.findViewById(R.id.layout_swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> scrollListener.reset());
        scrollListener.reset();
        textInfo = view.findViewById(R.id.text_info);
        return view;
    }

    private void loadData(int page) {
        reload = page == 1;
        adapter.setLoading(true);
        ReportRequestBody body = new ReportRequestBody();
        body.token = PreferencesUtil.getString(getContext(), Constant.PREF_TOKEN);
        body.page = page;
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -3);
        body.startDate = c.getTime().getTime();
        body.endDate = new Date().getTime();
        APIRequest<BPReportResponseBody> request = new APIRequest<>(getContext(),
                APIRequest.Instance.TRX_BP_REPORT, body, this, this);
        queue = Volley.newRequestQueue(getContext());
        queue.add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        adapter.setLoading(false);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResponse(BPReportResponseBody response) {
        swipeRefreshLayout.setRefreshing(false);
        adapter.setLoading(false);
        ResponseStatus status = ResponseStatus.getByValue(response.status);
        if (!Status.SUCCESS.equals(status.getStatus())) {
            Toast.makeText(getContext(), status.getMessageId(), Toast.LENGTH_SHORT).show();
            return;
        }
        if (reload) {
            adapter.clear();
            adapter.setTotalPage(response.totalPage);
            reload = false;
        }
        if (response.items.length > 0) {
            adapter.add(response.items);
            textInfo.setVisibility(View.GONE);
        } else {
            textInfo.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }
}
