package id.co.biggroup.bigpay.fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import id.co.biggroup.bigpay.util.CommonUtil;

/**
 * Created by developer on 4/27/18.
 */

public class BaseFrag extends Fragment implements Response.ErrorListener {
    protected RequestQueue queue;

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getContext(), CommonUtil.getErrorMessageResource(error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(request -> true);
        }
    }
}
