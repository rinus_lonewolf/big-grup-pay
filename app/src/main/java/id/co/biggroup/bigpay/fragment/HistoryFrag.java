package id.co.biggroup.bigpay.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.adapter.HistoryPagerAdapter;

public class HistoryFrag extends Fragment
{
  private HistoryPagerAdapter containerAdapter;
  private ViewPager viewPager;
  private TabLayout tabs;

  public HistoryFrag() { }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.frag_history, container, false);
    containerAdapter = new HistoryPagerAdapter(getActivity(), getChildFragmentManager());
    viewPager = view.findViewById(R.id.viewpager);
    viewPager.setAdapter(containerAdapter);
    tabs = view.findViewById(R.id.tabs);
    tabs.setupWithViewPager(viewPager);
    return view;
  }
}
