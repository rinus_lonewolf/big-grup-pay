package id.co.biggroup.bigpay.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.synnapps.carouselview.ImageListener;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.act.WebViewAct;
import id.co.biggroup.bigpay.adapter.GridMenuAdapter;
import id.co.biggroup.bigpay.api.response.ProfileResponseBody;
import id.co.biggroup.bigpay.databinding.FragMenuBinding;
import id.co.biggroup.bigpay.enums.HomeMenuItem;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.PreferencesUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFrag extends Fragment implements WidgetEventHandler {

    public final static String TYPE = "type";
    public final static int HOME = 1;
//    public final static int TRX = 2;

    private final static HomeMenuItem[] items_trx = new HomeMenuItem[] {
            HomeMenuItem.RESIDENTIAL_BILL, HomeMenuItem.BILL_PAYMENT,
            HomeMenuItem.QR_PAYMENT
    };
    private final static HomeMenuItem[] items_home = new HomeMenuItem[] {
      HomeMenuItem.BUILDING_INFO, HomeMenuItem.CHAT_ON_WA
    };
    private int[] sampleImages = {R.drawable.slide_1, R.drawable.slide_2, R.drawable.slide_3,
      R.drawable.slide_4, R.drawable.slide_5, R.drawable.slide_6, R.drawable.slide_7,
      R.drawable.slide_13, R.drawable.slide_14, R.drawable.slide_18, R.drawable.slide_19,
      R.drawable.slide_21, R.drawable.slide_22, R.drawable.slide_23 };

    private FragMenuBinding binding;
    private ImageListener imageListener = (position, imageView) ->
            imageView.setImageResource(sampleImages[position]);

    private int type;

    public MenuFrag() {
        // Required empty public constructor
    }

    public static MenuFrag newInstance(int type) {
        MenuFrag frag = new MenuFrag();
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
                type = getArguments().getInt(TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_menu, container, false);
        binding.setHandler(this);
        ProfileResponseBody profile = PreferencesUtil.getProfile(getContext());
        binding.setProfile(profile);
//        if (profile.img != null && !profile.img.equals(""))
//            binding.included.imgProfile.setImageBitmap(CommonUtil.decodeToBase64(profile.img));
//        if (type == TRX) {
//            items = items_trx;
//        }
        binding.trxViewGrid.setAdapter(new GridMenuAdapter(getContext(), items_trx));
        binding.trxViewGrid.setOnItemClickListener((adapterView, view, i, l) -> {
            HomeMenuItem item = (HomeMenuItem) adapterView.getAdapter().getItem(i);
            openIntent(item);
        });

        binding.homeViewGrid.setAdapter(new GridMenuAdapter(getContext(), items_home));
        binding.homeViewGrid.setOnItemClickListener((adapterView, view, i, l) -> {
            HomeMenuItem item = (HomeMenuItem) adapterView.getAdapter().getItem(i);
            openIntent(item);
        });

        //Hendric - Add Profile Picture Locally (20180625)
//        if (PreferencesUtil.getString(getContext(), "imageUri") != null) {
//            Glide.with(this)
//              .load(Uri.parse(PreferencesUtil.getString(getContext(), "imageUri")))
//              .into(binding.included.imgProfile);
//        }

        binding.carouselView.setPageCount(sampleImages.length);
        binding.carouselView.setImageListener(imageListener);

        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {
//        if (view.equals(binding.included.buttonTopup))
//        {
//            Intent i = new Intent(MenuFrag.this.getContext(), TopUpAct.class);
//            startActivity(i);
//        }
    }

    private void openIntent(HomeMenuItem item) {
        if (item.getActivity() == null && item.getUrl() == null) {
            //Hendric - 20180628 - Add info for unavailable feature
            Toast.makeText(MenuFrag.this.getContext(), getString(R.string.info_feature_not_available),
              Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent;
        if (item.getActivity() != null) {
            intent = new Intent(MenuFrag.this.getContext(), item.getActivity());
        } else {
            if (item.getUrl().contains("wa.me")) {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(item.getUrl()));
            }
            else {
                intent = new Intent(MenuFrag.this.getContext(), WebViewAct.class);
                intent.putExtra(Constant.DATA_ITEM, Constant.DATA_ITEM_TYPE_HOME);
                intent.putExtra(Constant.DATA_MENU_ITEM, item);
            }
        }
        startActivity(intent);
    }
}
