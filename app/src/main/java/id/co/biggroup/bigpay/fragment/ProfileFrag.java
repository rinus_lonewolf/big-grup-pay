package id.co.biggroup.bigpay.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.act.ChangeKeyAct;
import id.co.biggroup.bigpay.act.InitAct;
import id.co.biggroup.bigpay.act.TopUpAct;
import id.co.biggroup.bigpay.act.UpdateProfileAct;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.SignoutRequestBody;
import id.co.biggroup.bigpay.api.response.APIResponseBody;
import id.co.biggroup.bigpay.api.response.ProfileResponseBody;
import id.co.biggroup.bigpay.databinding.FragProfileBinding;
import id.co.biggroup.bigpay.enums.ResponseStatus;
import id.co.biggroup.bigpay.enums.Status;
import id.co.biggroup.bigpay.handler.WidgetEventHandler;
import id.co.biggroup.bigpay.util.CommonUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFrag extends BaseFrag implements WidgetEventHandler, Response.Listener<APIResponseBody> {
  private FragProfileBinding binding;
  private GoogleSignInClient mGoogleSignInClient;

  public ProfileFrag()
  {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    // Configure sign-in to request the user's ID, email address, and basic
    // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
      .requestEmail()
      .build();
    mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    binding = DataBindingUtil.inflate(inflater, R.layout.frag_profile, container, false);
    binding.setHandler(this);
    ProfileResponseBody profile = PreferencesUtil.getProfile(getContext());
    binding.setProfile(profile);
    Log.d("ProfileFrag","profile.img : "+profile.img);
    if (profile.img != null && !profile.img.equals(""))
      binding.included.imgProfile.setImageBitmap(CommonUtil.decodeToBase64(profile.img));
    return binding.getRoot();
  }

  @Override
  public void onClick(View view) {
    if (binding.progressBar.getVisibility() == View.VISIBLE) {
      return;
    }
//    binding.progressBar.setVisibility(View.VISIBLE);
    if (view.equals(binding.buttonChangekey)) {
      Intent i = new Intent(getContext(), ChangeKeyAct.class);
      startActivity(i);
    }
    else if (view.equals(binding.buttonUpdateprofile)) {
      Intent i = new Intent(getContext(), UpdateProfileAct.class);
      startActivity(i);
    }
    else if (view.equals(binding.buttonSignout)) {
      signOut();

      SignoutRequestBody body = new SignoutRequestBody();
      body.token = PreferencesUtil.getString(getContext(), Constant.PREF_TOKEN);
      APIRequest<APIResponseBody> request = new APIRequest<>(getContext(),
        APIRequest.Instance.SIGN_OUT, body, this, this);
      RequestQueue queue = Volley.newRequestQueue(getContext());
      queue.add(request);
    }
    else if (view.equals(binding.included.buttonTopup)) {
      Intent i = new Intent(getContext(), TopUpAct.class);
      startActivity(i);
    }
  }

  @Override
  public void onErrorResponse(VolleyError error) {
    super.onErrorResponse(error);
    binding.progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  public void onResponse(APIResponseBody response) {
    binding.progressBar.setVisibility(View.INVISIBLE);
    ResponseStatus status = ResponseStatus.getByValue(response.status);
    if (!Status.SUCCESS.equals(status.getStatus()))
    {
      Toast.makeText(getContext(), status.getMessageId(), Toast.LENGTH_SHORT).show();
      return;
    }
    PreferencesUtil.removeItems(getContext(), new String[]{Constant.PREF_TOKEN, Constant.PREF_NAME, Constant.PREF_BALANCE});
    Intent i = new Intent(getContext(), InitAct.class);
    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(i);
  }

  private void signOut()
  {
    GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());
    if (account != null)
    {
      mGoogleSignInClient.signOut()
        .addOnCompleteListener(getActivity(), task -> {
          Toast.makeText(getActivity(), "Google Sign Out Success", Toast.LENGTH_SHORT).show();
        });
    }

    AccessToken accessToken = AccessToken.getCurrentAccessToken();
    if (accessToken != null && !accessToken.isExpired())
    {
      LoginManager.getInstance().logOut();
      Toast.makeText(getActivity(), "Facebook Sign Out Success", Toast.LENGTH_SHORT).show();
    }
  }
}
