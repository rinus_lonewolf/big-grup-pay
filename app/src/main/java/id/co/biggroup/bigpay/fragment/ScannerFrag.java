package id.co.biggroup.bigpay.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.act.ScanAct;
import id.co.biggroup.bigpay.api.request.APIRequest;
import id.co.biggroup.bigpay.api.request.QRCodeDataRequestBody;
import id.co.biggroup.bigpay.api.response.QRPayResponseBody;
import id.co.biggroup.bigpay.form.QRInputForm;
import id.co.biggroup.bigpay.util.AuthUtil;
import id.co.biggroup.bigpay.util.CommonUtil;
import id.co.biggroup.bigpay.util.PreferencesUtil;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by dolan on 2/21/18.
 */

public class ScannerFrag extends Fragment implements ZXingScannerView.ResultHandler,
  Response.Listener<QRPayResponseBody>, Response.ErrorListener
{
  private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
  private ZXingScannerView scannerView;
  private RequestQueue queue;
  private Context ctx;
  private ProgressDialog progressDialog;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
  {
    ctx = getContext();
    scannerView = new ZXingScannerView(ctx);
    BarcodeFormat[] formats = {BarcodeFormat.QR_CODE};
    scannerView.setFormats(Arrays.asList(formats));
    progressDialog = new ProgressDialog(getContext());
    progressDialog.setMessage(getString(R.string.info_please_wait));
    progressDialog.setCancelable(false);

    return scannerView;
  }

  @Override
  public void onResume()
  {
    super.onResume();
    scannerView.setResultHandler(this);
    scannerView.setBorderColor(Color.WHITE);
    scannerView.setLaserEnabled(false);
    scannerView.startCamera();
  }

  @Override
  public void onPause()
  {
    super.onPause();
    scannerView.stopCamera();
  }

  @Override
  public void onStop()
  {
    super.onStop();
    if (queue != null)
      queue.cancelAll(request -> true);
  }

  @SuppressLint("HardwareIds")
  @Override
  public void handleResult(Result result)
  {
    progressDialog.show();

    QRCodeDataRequestBody body = new QRCodeDataRequestBody();
    body.qrCodeString = result.toString();
    body.userEmailApps = PreferencesUtil.getString(ctx, Constant.PREF_EMAIL);
    body.customerName = PreferencesUtil.getString(ctx, Constant.PREF_NAME);
    body.deviceId = Settings.Secure.getString(ctx.getContentResolver(),
      Settings.Secure.ANDROID_ID);
    body.paymentGatewayId = ctx.getString(R.string.pg_id);
    body.checksum = AuthUtil.digest(body.flatten() + ctx.getString(R.string.pg_key));

    APIRequest<QRPayResponseBody> request = new APIRequest<>(ctx,
      APIRequest.Instance.TRX_QR_DATA, body, this, this, "QR");
    queue = Volley.newRequestQueue(ctx);
    queue.add(request);
  }

  @Override
  public void onResponse(QRPayResponseBody response)
  {
    progressDialog.hide();

    if (!response.responseCode.equals("PL00GQD"))
    {
      Toast.makeText(ctx, R.string.info_unrecognized_qr, Toast.LENGTH_SHORT).show();
      onPause();
      onResume();
    }
    else
    {
      QRInputForm form = new QRInputForm();
      try
      {
        form.merchantName = response.merchantName;
        form.date = dateFormat.parse(response.checkoutDate);
        form.currency = response.checkoutCurrency;
        form.ecommRefNo = response.ecommRefNo;
      }
      catch (ParseException e)
      {
        e.printStackTrace();
      }

      Intent i = new Intent(ctx, ScanAct.class);
      i.putExtra(Constant.DATA_STEP, Constant.STEP_INPUT);
      i.putExtra(Constant.DATA_FORM, form);
      ctx.startActivity(i);
    }
  }

  @Override
  public void onErrorResponse(VolleyError error)
  {
    progressDialog.hide();
    Toast.makeText(ctx, CommonUtil.getErrorMessageResource(error), Toast.LENGTH_SHORT).show();
  }
}
