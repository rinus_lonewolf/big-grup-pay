package id.co.biggroup.bigpay.handler;

public interface AsyncResponse {
  void processError();
  void processFinish(String output);
}
