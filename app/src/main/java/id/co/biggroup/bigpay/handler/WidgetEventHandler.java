package id.co.biggroup.bigpay.handler;

import android.view.View;

/**
 * Created by developer on 2/21/18.
 */

public interface WidgetEventHandler {
    void onClick(View view);
}
