package id.co.biggroup.bigpay.listener;

import android.widget.ExpandableListView;

/**
 * Created by developer on 4/17/18.
 */

public class ELVExpandListener implements ExpandableListView.OnGroupExpandListener {
    private ExpandableListView view;
    private int lastGroup = -1;

    public ELVExpandListener(ExpandableListView view) {
        this.view = view;
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        if (groupPosition != lastGroup) {
            view.collapseGroup(lastGroup);
        }
        lastGroup = groupPosition;
    }
}
