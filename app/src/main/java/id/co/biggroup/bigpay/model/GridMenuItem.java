package id.co.biggroup.bigpay.model;

/**
 * Created by developer on 3/22/18.
 */

public interface GridMenuItem {
    int getIconResourceId();
    int getLabelResourceId();
    int getId();
}
