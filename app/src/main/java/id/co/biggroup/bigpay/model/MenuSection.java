package id.co.biggroup.bigpay.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import id.co.biggroup.bigpay.R;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by developer on 3/23/18.
 */

public class MenuSection<T extends GridMenuItem> extends StatelessSection {

    private int titleResId;
    private List<T> items;
    private Listener<T> listener;

    public MenuSection(int titleResId, List<T> items, Listener<T> listener) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.item_grid)
                .headerResourceId(R.layout.header_grid)
                .build());

        this.titleResId = titleResId;
        this.items = items;
        this.listener = listener;
    }

    public MenuSection(int titleResId, T[] items, Listener<T> listener) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.item_grid)
                .headerResourceId(R.layout.header_grid)
                .build());

        this.titleResId = titleResId;
        this.items = Arrays.asList(items);
        this.listener = listener;
    }

    @Override
    public int getContentItemsTotal() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        final GridMenuItem item = items.get(position);
        itemViewHolder.imageView.setImageResource(item.getIconResourceId());
        itemViewHolder.textView.setText(item.getLabelResourceId());
        itemViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemSelected((T) item);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        headerViewHolder.textView.setText(titleResId);
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        HeaderViewHolder(View view) {
            super(view);

            textView = view.findViewById(android.R.id.text1);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textView;
        private View rootView;

        ItemViewHolder(View view) {
            super(view);

            rootView = view;
            imageView = view.findViewById(R.id.imageView);
            textView = view.findViewById(R.id.textView);
        }
    }

    public interface Listener<T extends GridMenuItem> {
        void onItemSelected(T item);
    }
}
