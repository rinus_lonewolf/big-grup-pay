package id.co.biggroup.bigpay.model;

/**
 * Created by developer on 3/22/18.
 */

public interface SpinnerItem {
    int getId();
    String getLabel();
}
