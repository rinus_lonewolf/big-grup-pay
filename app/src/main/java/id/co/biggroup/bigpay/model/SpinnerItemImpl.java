package id.co.biggroup.bigpay.model;

import java.io.Serializable;

/**
 * Created by developer on 3/22/18.
 */

public class SpinnerItemImpl implements SpinnerItem, Serializable {
    private int id;
    private String label;

    public SpinnerItemImpl(int id, String label) {
        this.id = id;
        this.label = label;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
