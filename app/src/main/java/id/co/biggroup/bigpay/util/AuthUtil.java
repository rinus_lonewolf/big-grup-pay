package id.co.biggroup.bigpay.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by developer on 4/25/18.
 */

public class AuthUtil {
    private final static String SHA_256 = "SHA-256";

    public static String digest(String plain) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance(SHA_256);
            digest.reset();
            digest.update(plain.getBytes());
            result = toHex(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String toHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }
}
