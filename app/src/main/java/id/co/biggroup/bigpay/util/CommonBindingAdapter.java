package id.co.biggroup.bigpay.util;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import id.co.biggroup.bigpay.adapter.SpinnerAdapter;
import id.co.biggroup.bigpay.model.SpinnerItem;

/**
 * Created by dolan on 3/18/18.
 */

public class CommonBindingAdapter {

    public interface Listener {
        void onItemSelected(AdapterView<?> parent, View view, int position, long id);
    }

    @BindingConversion
    public static String toString(Long value) {
        return value != null ? value.toString() : "";
    }

    @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
    public static Long getLongValue(EditText editText) {
        long value = 0;
        try {
            value = Long.parseLong(editText.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return value;
    }

    @BindingAdapter(value = {"selectedValue", "selectedValueAttrChanged", "itemSelectedListener"}, requireAll = false)
    public static void bindData(Spinner spinner, String newValue, final InverseBindingListener ibListener, final Listener listener) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (ibListener != null) {
                    ibListener.onChange();
                }
                if (listener != null) {
                    listener.onItemSelected(parent, view, position, id);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                if (ibListener != null) {
                    ibListener.onChange();
                }
            }
        });
        if (newValue != null) {
            int pos = ((ArrayAdapter<String>) spinner.getAdapter()).getPosition(newValue);
            spinner.setSelection(pos);
        }
    }

    @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
    public static String getSelectedString(Spinner spinner) {
        return (String) spinner.getSelectedItem();
    }

    @BindingAdapter(value = {"selectedValue", "selectedValueAttrChanged", "itemSelectedListener"}, requireAll = false)
    public static void bindData(Spinner spinner, SpinnerItem newValue, InverseBindingListener ibListener, Listener listener) {
        _bindData(spinner, newValue, ibListener, listener);
    }

    @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
    public static SpinnerItem getSelectedSpinnerItem(Spinner spinner) {
        return (SpinnerItem) spinner.getSelectedItem();
    }

    private static void _bindData(final Spinner spinner, final SpinnerItem newValue, final InverseBindingListener ibListener, final Listener listener) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (ibListener != null) {
                    ibListener.onChange();
                }
                if (listener != null) {
                    listener.onItemSelected(parent, view, position, id);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                if (ibListener != null) {
                    ibListener.onChange();
                }
            }
        });
        if (newValue != null) {
            int pos = ((SpinnerAdapter) spinner.getAdapter()).getPosition(newValue);
            spinner.setSelection(pos);
        }
    }
}
