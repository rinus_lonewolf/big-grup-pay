package id.co.biggroup.bigpay.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 4/5/18.
 */

public class CommonUtil {

    private final static SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault());
    private final static SimpleDateFormat DATE_TIME_FORMAT_REFERENCE = new SimpleDateFormat("yyyyMMddHHmmssSSSS", Locale.getDefault());
    private final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
    private final static String IDR = "Rp ";

    public static int getErrorMessageResource(VolleyError error) {
        if (error instanceof TimeoutError) {
            return R.string.notif_timeout;
        } else if (error instanceof NoConnectionError) {
            return R.string.notif_no_connection;
        } else if (error instanceof AuthFailureError) {
            return R.string.notif_auth_failure;
        } else if (error instanceof ServerError) {
            return R.string.notif_server_error;
        } else if (error instanceof NetworkError) {
            return R.string.notif_network_error;
        } else if (error instanceof ParseError) {
            return R.string.notif_parse_error;
        }
        return R.string.notif_other_error;
    }

    public static String formatDateTime(Date date) {
        return DATE_TIME_FORMAT.format(date);
    }

    public static String formatReferenceDateTime(Date date) {
        return DATE_TIME_FORMAT_REFERENCE.format(date);
    }

    public static String formatCurrency(Long number) {
        if (number == null) {
            return  IDR + "0";
        }
        return IDR + NUMBER_FORMAT.format(number);
    }

    public static String formatCurrencyDecimal(BigDecimal number) {
        if (number == null) {
            return  IDR + "0";
        }
        return IDR + NUMBER_FORMAT.format(number);
    }

    public static String formatHistoryCurrency(Long number, String type) {
        String curr = null;
        if (number == null)
            return null;

        if (type.equals("Db"))
            curr = "+ " + IDR + NUMBER_FORMAT.format(number);
        else if (type.equals("Cr"))
            curr = "- " + IDR + NUMBER_FORMAT.format(number);

        return curr;
    }

    public static String formatVA(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        char[] chars = input.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            builder.append(chars[i]);
            if (i + 1 < chars.length && (i + 1) % 4 == 0) {
                builder.append(" - ");
            }
        }
        return builder.toString();
    }

    public static String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b,Base64.DEFAULT);
    }

    public static Bitmap decodeToBase64(String strBase64) {
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static Bitmap resizeBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static File createImageFile() throws IOException
    {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
          Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
          imageFileName,  // prefix
          ".jpg",   // suffix
          storageDir      // directory
        );

        return image;
    }
}
