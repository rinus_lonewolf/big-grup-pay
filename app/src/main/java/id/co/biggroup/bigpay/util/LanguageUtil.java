package id.co.biggroup.bigpay.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.R;

public class LanguageUtil
{
  public static final String Indonesian = "in";
  public static final String English = "en";

  public static void updateBaseContextLocale(Context context, String language)
  {
    Locale locale = new Locale(language);
    Locale.setDefault(locale);
    updateResourcesLocaleLegacy(context, locale);
  }

  private static void updateResourcesLocaleLegacy(Context context, Locale locale)
  {
    Resources resources = context.getResources();
    Configuration configuration = resources.getConfiguration();
    configuration.locale = locale;
    resources.updateConfiguration(configuration, resources.getDisplayMetrics());
  }

  private static void changeStyleLanguage(Context ctx, Button activeButton, Button inactiveButton, View parentOn, View parentOff)
  {
    inactiveButton.setTextColor(ctx.getResources().getColor(R.color.colorPrimaryDark));
    inactiveButton.setBackgroundColor(Color.TRANSPARENT);
    parentOff.setBackgroundResource(R.drawable.button_wrapper_language2);

    activeButton.setTextColor(ctx.getResources().getColor(R.color.light_grey));
    activeButton.setBackgroundResource(R.drawable.button_language);
    parentOn.setBackgroundResource(R.drawable.button_wrapper_language);
  }

  public static void checkInitLanguage(Context ctx, Button btnId, Button btnEn, View layoutId, View layoutEn)
  {
    String language = PreferencesUtil.getString(ctx, Constant.PREF_LANGUAGE);

    if (language != null && !language.equals(""))
    {
      if (language.equals(Indonesian))
        LanguageUtil.changeStyleLanguage(ctx, btnId, btnEn, layoutId, layoutEn);
      else
        LanguageUtil.changeStyleLanguage(ctx, btnEn, btnId, layoutEn, layoutId);
    }
    else
    {
      if (Locale.getDefault().getLanguage().equals(Indonesian))
      {
        PreferencesUtil.save(ctx, Constant.PREF_LANGUAGE, Indonesian);
        LanguageUtil.changeStyleLanguage(ctx, btnId, btnEn, layoutId, layoutEn);
      }
      else
      {
        PreferencesUtil.save(ctx, Constant.PREF_LANGUAGE, English);
        LanguageUtil.changeStyleLanguage(ctx, btnEn, btnId, layoutEn, layoutId);
      }
    }
  }

  public static void checkLanguage(Context ctx)
  {
    String language = PreferencesUtil.getString(ctx, Constant.PREF_LANGUAGE);

    if (language != null && !language.equals(""))
    {
      if (language.equals(Indonesian))
        LanguageUtil.updateBaseContextLocale(ctx, Indonesian);
      else
        LanguageUtil.updateBaseContextLocale(ctx, English);
    }
  }
}
