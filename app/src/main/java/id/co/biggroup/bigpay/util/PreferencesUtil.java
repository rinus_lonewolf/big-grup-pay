package id.co.biggroup.bigpay.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import id.co.biggroup.bigpay.Constant;
import id.co.biggroup.bigpay.api.response.ProfileResponseBody;

/**
 * Created by developer on 4/25/18.
 */

public class PreferencesUtil {

    public static void save(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void save(Context context, Map<String, Object> values) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            if (entry.getValue() instanceof  String) {
                editor.putString(entry.getKey(), (String) entry.getValue());
            }
            if (entry.getValue() instanceof Long) {
                editor.putLong(entry.getKey(), (Long) entry.getValue());
            }
        }
        editor.apply();
    }

    public static String getString(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }

    public static Long getLong(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        return prefs.getLong(key, 0L);
    }

    public static ProfileResponseBody getProfile(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        ProfileResponseBody profile = new ProfileResponseBody();
        profile.balance = prefs.getLong(Constant.PREF_BALANCE, 0L);
        profile.email = prefs.getString(Constant.PREF_EMAIL, null);
        profile.name = prefs.getString(Constant.PREF_NAME, null);
        profile.phone = prefs.getString(Constant.PREF_PHONE, null);
        profile.img = prefs.getString(Constant.PREF_IMAGE, null);

        return profile;
    }

    public static void remove(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
    }

    public static void removeItems(Context context, String[] keys) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.KEY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        for (String key : keys) {
            editor.remove(key);
        }
        editor.apply();
    }
}
