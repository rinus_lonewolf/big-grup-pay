package id.co.biggroup.bigpay.util;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 3/26/18.
 */

public class ValidationUtil {
    private Context context;
    public final static Pattern EMAIL_PATTERN = Pattern.compile("^([a-zA-Z0-9_\\-.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})$");
    public final static Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$");

    public enum LengthType {
        MIN, MAX
    }

    public ValidationUtil(Context context) {
        this.context = context;
    }

    public boolean isFilled(TextInputEditText field) {
        String value = field.getText().toString();
        TextInputLayout layout = (TextInputLayout) field.getParent().getParent();

        if (StringUtils.isBlank(value)) {
            layout.setError(context.getResources().getString(R.string.field_required));
            layout.setErrorEnabled(true);
            return false;
        }
        layout.setErrorEnabled(false);
        return true;
    }

    public boolean isEqual(TextInputEditText target, TextInputEditText reference) {
        String targetVal = target.getText().toString();
        String refVal = reference.getText().toString();
        TextInputLayout layout = (TextInputLayout) target.getParent().getParent();

        if (!targetVal.equals(refVal)) {
            layout.setError(context.getResources().getString(R.string.field_must_equal));
            layout.setErrorEnabled(true);
            return false;
        }
        layout.setErrorEnabled(false);
        return true;
    }

    public boolean isKeysDiffer(TextInputEditText target, TextInputEditText reference) {
        String targetVal = target.getText().toString();
        String refVal = reference.getText().toString();
        TextInputLayout layout = (TextInputLayout) target.getParent().getParent();

        if (targetVal.equals(refVal)) {
            layout.setError(context.getResources().getString(R.string.field_keys_must_differ));
            layout.setErrorEnabled(true);
            return false;
        }
        return true;
    }

    public boolean isPatternValid(TextInputEditText field, Pattern pattern) {
        String value = field.getText().toString();
        TextInputLayout layout = (TextInputLayout) field.getParent().getParent();

        if (!pattern.matcher(value).matches()) {
            int resid = R.string.field_invalid_email;
            if (PASSWORD_PATTERN.equals(pattern)) {
                resid = R.string.field_invalid_password;
            }
            layout.setError(context.getResources().getString(resid));
            layout.setErrorEnabled(true);
            return false;
        }
        layout.setErrorEnabled(false);
        return true;
    }

    public boolean isLengthValid(TextInputEditText field, LengthType type, int length) {
        String value = field.getText().toString();
        TextInputLayout layout = (TextInputLayout) field.getParent().getParent();

        if (type == LengthType.MIN && value.length() < length) {
            layout.setError(String.format(context.getResources().getString(R.string.field_invalid_min_length), length));
            layout.setErrorEnabled(true);
            return false;
        }
        if (type == LengthType.MAX && value.length() > length) {
            layout.setError(String.format(context.getResources().getString(R.string.field_invalid_max_length), length));
            layout.setErrorEnabled(true);
            return false;
        }
        layout.setErrorEnabled(false);
        return true;
    }
}
