package id.co.biggroup.bigpay.util;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import id.co.biggroup.bigpay.BuildConfig;
import id.co.biggroup.bigpay.handler.AsyncResponse;

public class VersionChecker extends AsyncTask<String, String, String>
{
  public AsyncResponse response;
  private String onlineVersion;

  public VersionChecker (AsyncResponse response)
  {
    this.response = response;
  }

  @Override
  protected String doInBackground(String... strings)
  {
    try {
      Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" +
        BuildConfig.APPLICATION_ID + "&hl=en")
        .timeout(10000)
        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
        .referrer("http://www.google.com")
        .get();
      if (document != null) {
        Elements element = document.getElementsContainingOwnText("Current Version");
        for (Element ele : element) {
          if (ele.siblingElements() != null) {
            Elements sibElements = ele.siblingElements();
            for (Element se : sibElements) {
              onlineVersion = se.text().trim();
            }
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      response.processError();
    }
    return onlineVersion;
  }

  @Override
  protected void onPostExecute(String onlineVersion) {
    if (onlineVersion != null) {
      Log.d("PLAYSTORE VERSION", onlineVersion);
      response.processFinish(onlineVersion);
    }
  }

}
