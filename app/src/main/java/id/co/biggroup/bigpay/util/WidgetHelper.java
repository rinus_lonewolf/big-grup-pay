package id.co.biggroup.bigpay.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import id.co.biggroup.bigpay.R;
import id.co.biggroup.bigpay.model.GridMenuItem;

/**
 * Created by developer on 4/4/18.
 */

public class WidgetHelper {
    private final static int COLUMN = 3;

    public interface Listener<T extends GridMenuItem> {
        void onItemSelected(T t);
    }

    public static <T extends GridMenuItem> void fill(TableLayout layout, T[] items, final Listener listener, int hLabelResId, int bgResId) {
        Context context = layout.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        TableRow.LayoutParams params = new TableRow.LayoutParams();
        params.weight = 1;
        params.width = 0;
        params.height = TableRow.LayoutParams.WRAP_CONTENT;
        int px = context.getResources().getDimensionPixelSize(R.dimen.len_8dp);
        params.setMargins(px, px, px, px);

        TextView header = new TextView(context);
        header.setText(hLabelResId);
        header.setLayoutParams(params);
        header.setTypeface(null, Typeface.BOLD);
        TableRow row = new TableRow(context);
        row.addView(header);
        layout.addView(row);

        if (bgResId != 0) {
            layout.setBackgroundResource(bgResId);
            header.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        }
        for (int i = 0; i < items.length; i++) {
            if (i % COLUMN == 0) {
                row = new TableRow(context);
                layout.addView(row);
            }
            final GridMenuItem item = items[i];
            View view = inflater.inflate(R.layout.item_grid, row, false);
            view.setLayoutParams(params);
            ImageView imageView = view.findViewById(R.id.imageView);
            imageView.setImageResource(item.getIconResourceId());
            TextView textView = view.findViewById(R.id.textView);
            textView.setText(item.getLabelResourceId());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemSelected(item);
                }
            });
            row.addView(view);
        }
    }
}
