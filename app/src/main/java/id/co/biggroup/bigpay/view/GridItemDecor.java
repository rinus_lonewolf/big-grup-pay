package id.co.biggroup.bigpay.view;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

/**
 * Created by dolan on 3/24/18.
 */

public class GridItemDecor extends RecyclerView.ItemDecoration {
    private int space;
    private int halfSpace;
    private int spanSize;
    private SectionedRecyclerViewAdapter adapter;

    public GridItemDecor(int space, int spanSize, SectionedRecyclerViewAdapter adapter) {
        this.space = space;
        this.halfSpace = space/2;
        this.spanSize = spanSize;
        this.adapter = adapter;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = space;
        parent.getAdapter();

        int pos = parent.getChildAdapterPosition(view);
        if (adapter.getSectionItemViewType(pos) != SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER) {
            int col = adapter.getPositionInSection(pos) % spanSize;
            if (col == 0) {
                outRect.left = 0;
            } else {
                outRect.left = halfSpace;
            }
            if (col == spanSize - 1) {
                outRect.right = 0;
            } else {
                outRect.right = halfSpace;
            }
        }
        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = space;
        } else {
            outRect.top = 0;
        }
    }
}
