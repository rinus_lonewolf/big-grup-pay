package id.co.biggroup.bigpay.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.view.View;
import android.widget.TextView;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 2/26/18.
 */

public class KeyboardView extends FrameLayout implements View.OnClickListener {
    private TextView field;

    public KeyboardView(@NonNull Context context) {
        super(context);
        init();
    }

    public KeyboardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyboardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.num_input, this);
        field = findViewById(R.id.field);
        findViewById(R.id.key_0).setOnClickListener(this);
        findViewById(R.id.key_1).setOnClickListener(this);
        findViewById(R.id.key_2).setOnClickListener(this);
        findViewById(R.id.key_3).setOnClickListener(this);
        findViewById(R.id.key_4).setOnClickListener(this);
        findViewById(R.id.key_5).setOnClickListener(this);
        findViewById(R.id.key_6).setOnClickListener(this);
        findViewById(R.id.key_7).setOnClickListener(this);
        findViewById(R.id.key_8).setOnClickListener(this);
        findViewById(R.id.key_9).setOnClickListener(this);
        findViewById(R.id.key_b).setOnClickListener(this);
        findViewById(R.id.key_c).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String tag = getResources().getString(R.string.tag_num);
        if (tag.equals(view.getTag())) {
            TextView tv = (TextView) view;
            field.append(tv.getText());
            return;
        }
        switch (view.getId()) {
            case R.id.key_c:
                field.setText(null);
                break;
            case R.id.key_b:
                CharSequence sequence = field.getText();
                int length = sequence.length();
                if (length > 0) {
                    field.setText(sequence.subSequence(0, length - 1));
                }
                break;
        }
    }
}
