package id.co.biggroup.bigpay.view;

import android.content.Context;
import android.widget.ExpandableListView;

/**
 * Created by developer on 4/10/18.
 */

public class MasterELV extends ExpandableListView {
    public MasterELV(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
