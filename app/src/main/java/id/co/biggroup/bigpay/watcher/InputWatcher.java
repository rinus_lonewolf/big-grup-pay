package id.co.biggroup.bigpay.watcher;

import android.app.Activity;
import android.databinding.ObservableField;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import org.apache.commons.lang3.StringUtils;

import id.co.biggroup.bigpay.R;

/**
 * Created by developer on 2/21/18.
 */

public class InputWatcher implements TextWatcher {
    private Activity activity;
    private EditText view;
    private ObservableField<String> field;

    public InputWatcher(Activity activity, EditText view, ObservableField<String> field) {
        this.view = view;
        this.activity = activity;
        this.field = field;
        view.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String value = s.toString();
        field.set(value);
        TextInputLayout layout = (TextInputLayout) view.getParent().getParent();
        if (StringUtils.isBlank(value)) {
            requestFocus(view);
            layout.setError(activity.getString(R.string.field_required));
            layout.setErrorEnabled(true);
        } else {
            layout.setErrorEnabled(false);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
